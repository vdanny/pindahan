	<?php include('partials/header.php'); ?>

	<div class="container">
		
		<form class="form-horizontal" id="form-signin" method="POST" action="<?php echo site_url('signin/doLogin'); ?>">
			<h2 align="center">Masuk</h2><br><br><br>
			<div class="form-group">
				<div class="col-md-4 col-md-offset-4">
					<input type="text" name="email" class="form-control" placeholder="Email" autocomplete="off">
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-4 col-md-offset-4">
					<input type="password" name="password" class="form-control" placeholder="Password" autocomplete="off">
				</div>
			</div>

			<div class="text-center">
				<p>Belum terdaftar sebagai member Pindahan? <a href="<?php echo site_url('signup') ?>">Daftar sekarang juga</a></p>
				<br><br>
				<button type="submit" class="btn1 btn-blue">Masuk</button>
				<button type="reset" class="btn1 btn-blue-fill">Reset</button>
			</div>
		</form>

	</div>

	<?php include('partials/footer.php'); ?>