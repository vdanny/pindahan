
	<?php include('partials/header.php'); ?>

	<div class="container">
		
		<input type="hidden" id="price_per_km" value="<?php echo $distance_price->price_per_km; ?>">

		<form class="form-horizontal" id="form-order" method="POST" action="<?php echo site_url('order/submit'); ?>">
			<fieldset>
				<!-- Form Name -->
				<h2 align="center">Pesan Pindahan</h2><br><br><br>

				<section id="datapindahan">
					<legend>Informasi Pindahan</legend>

					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-4 control-label" for="fullname">Nama Lengkap</label>
						<div class="col-md-5">
							<input id="fullname" name="fullname" placeholder="Nama lengkap anda" class="form-control input-md" required="" maxlength="250" type="text" value="<?php echo $this->session->has_userdata('user')? $this->session->userdata('user')->fullname : ''; ?>">
						</div>
					</div>

					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-4 control-label" for="phone">Nomor Kontak</label>
						<div class="col-md-5">
							<input id="phone" name="phone" placeholder="Nomor telepon atau handphone" class="form-control input-md" required="" maxlength="20" type="text" value="<?php echo $this->session->has_userdata('user')? $this->session->userdata('user')->phone : ''; ?>">
							<span class="help-block">ex: 0215123123 atau 081234567</span>
						</div>
					</div>

					<!-- Textarea -->
					<!-- <div class="form-group">
						<label class="col-md-4 control-label" for="pickup-address">Alamat Penjemputan</label>
						<div class="col-md-4">
							<textarea class="form-control" id="pickup-address" name="pickup_address" placeholder="Alamat lengkap lokasi penjemputan" required=""><?php echo $this->session->has_userdata('user')? $this->session->userdata('user')->address: ''; ?></textarea>
						</div>
					</div> -->

					<!-- Textarea -->
					<!-- <div class="form-group">
						<label class="col-md-4 control-label" for="destination-address">Alamat Tujuan</label>
						<div class="col-md-4">
							<textarea class="form-control" id="destination-address" name="destination_address" placeholder="Alamat lengkap lokasi tujuan" required=""></textarea>
						</div>
					</div> -->

					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-4 control-label" for="moving-date">Tanggal Pindahan</label>
						<div class="col-md-4">
							<input id="moving-date" name="moving_date" placeholder="Tanggal pindahan" class="form-control input-md datepicker" required="" readonly="" type="text" value="<?php $time = strtotime("+1 month", time()); echo date('m/d/Y', $time); ?>">
						</div>
					</div>

					<!-- Shift -->
					<div class="form-group">
						<label class="col-md-4 control-label" for="moving_time">Waktu Pindahan</label>
						<div class="col-md-4 input-group bootstrap-timepicker timepicker" style="padding-left: 15px;">
							<input id="timepicker2" type="text" class="form-control input-md" id="moving_time" name="moving_time" value="07.00">
				            <span class="input-group-addon">
				                <i class="glyphicon glyphicon-time"></i>
				            </span>
						</div>
					</div>


					<h4>Prakiraan Jarak</h4>

					<div class="form-group">
						<div class="col-md-10 col-md-offset-1">
							<input id="origin-input" name="pickup_address" class="controls" type="text" placeholder="Alamat penjemputan">
						    <input id="destination-input" name="destination_address" class="controls" type="text" placeholder="Alamat tujuan">

							<div id="map"></div>
							<h5 id="distance-approx">Anda belum memasukkan lokasi penjemputan dan tujuan.</h5>
							<input type="hidden" id="distance" name="distance" value="" required=""></input>
						</div>
					</div>
					<br><br>
				</section>

				<section id="harga">

					<legend>Pilihan Layanan</legend>

					<div class="form-group">

					<!-- Tabs Package & Custom -->
					<ul class="nav nav-tabs" role="tablist">
						<li role="presentation" class="active" id='package-price-tab'><a href="#package" aria-controls="package" role="tab" data-toggle="tab">Harga Paket</a></li>
						<li role="presentation" id='custom-price-tab'><a href="#custom" aria-controls="custom" role="tab" data-toggle="tab">Harga Custom</a></li>
					</ul>
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane active" id="package">
							<h3 align="center">Pilihan Paket</h3>
							<table class="table table-package">
								<thead>
									<tr>
										<th></th>
										<th class="bg-bronze"><?php echo $packages[0]->name; ?></th>
										<th class="bg-silver"><?php echo $packages[1]->name; ?></th>
										<th class="bg-gold"><?php echo $packages[2]->name; ?></th>
										<th class="bg-platinum"><?php echo $packages[3]->name; ?></th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<th scope="row">Transportasi</th>
										<td class="bg-bronze"><?php echo $packages[0]->transportation; ?> mobil box <br>+ gratis <?php echo $packages[0]->distance; ?> km pertama</td>
										<td class="bg-silver"><?php echo $packages[1]->transportation; ?> mobil box <br>+ gratis <?php echo $packages[1]->distance; ?> km pertama</td>
										<td class="bg-gold"><?php echo $packages[2]->transportation; ?> mobil box <br>+ gratis <?php echo $packages[2]->distance; ?> km pertama</td>
										<td class="bg-platinum"><?php echo $packages[3]->transportation; ?> mobil box <br>+ gratis <?php echo $packages[3]->distance; ?> km pertama</td>
									</tr>
									<tr>
										<th scope="row">Tenaga Angkat</th>
										<td class="bg-bronze"><?php echo $packages[0]->move_emp > 0? $packages[0]->move_emp . ' orang' : '-'; ?></td>
										<td class="bg-silver"><?php echo $packages[1]->move_emp > 0? $packages[1]->move_emp . ' orang' : '-'; ?></td>
										<td class="bg-gold"><?php echo $packages[2]->move_emp > 0? $packages[2]->move_emp . ' orang' : '-'; ?></td>
										<td class="bg-platinum"><?php echo $packages[3]->move_emp > 0? $packages[3]->move_emp . ' orang' : '-'; ?></td>
									</tr>
									<tr>
										<th scope="row">Tenaga Packing</th>
										<td class="bg-bronze"><?php echo $packages[0]->pack_emp > 0? $packages[0]->pack_emp . ' orang' : '-'; ?></td>
										<td class="bg-silver"><?php echo $packages[1]->pack_emp > 0? $packages[1]->pack_emp . ' orang' : '-'; ?></td>
										<td class="bg-gold"><?php echo $packages[2]->pack_emp > 0? $packages[2]->pack_emp . ' orang' : '-'; ?></td>
										<td class="bg-platinum"><?php echo $packages[3]->pack_emp > 0? $packages[3]->pack_emp . ' orang' : '-'; ?></td>
									</tr>
									<tr>
										<th scope="row">Tenaga Unpacking + Penataan</th>
										<td class="bg-bronze"><?php echo $packages[0]->unpack_emp > 0? $packages[0]->unpack_emp . ' orang' : '-'; ?></td>
										<td class="bg-silver"><?php echo $packages[1]->unpack_emp > 0? $packages[1]->unpack_emp . ' orang' : '-'; ?></td>
										<td class="bg-gold"><?php echo $packages[2]->unpack_emp > 0? $packages[2]->unpack_emp . ' orang' : '-'; ?></td>
										<td class="bg-platinum"><?php echo $packages[3]->unpack_emp > 0? $packages[3]->unpack_emp . ' orang' : '-'; ?></td>
									</tr>
									<tr>
										<th scope="row">Harga</th>
										<td class="bg-bronze">Rp <b><?php echo number_format($packages[0]->price, 0, '', '.') . ',-'; ?></b></td>
										<td class="bg-silver">Rp <b><?php echo number_format($packages[1]->price, 0, '', '.') . ',-'; ?></b></td>
										<td class="bg-gold">Rp <b><?php echo number_format($packages[2]->price, 0, '', '.') . ',-'; ?></b></td>
										<td class="bg-platinum">Rp <b><?php echo number_format($packages[3]->price, 0, '', '.') . ',-'; ?></b></td>
									</tr>
									<tr>
										<th scope="row"></th>
										<td class="bg-bronze"><button type="button" class="btn1 btn-white-fill btn-package" data-id="<?php echo $packages[0]->id; ?>" data-price="<?php echo $packages[0]->price; ?>" data-distance="<?php echo $packages[0]->distance * 1000; ?>">Pilih</button></td>
										<td class="bg-silver"><button type="button" class="btn1 btn-white-fill btn-package" data-id="<?php echo $packages[1]->id; ?>" data-price="<?php echo $packages[1]->price; ?>" data-distance="<?php echo $packages[1]->distance * 1000; ?>">Pilih</button></td>
										<td class="bg-gold"><button type="button" class="btn1 btn-white-fill btn-package" data-id="<?php echo $packages[2]->id; ?>" data-price="<?php echo $packages[2]->price; ?>" data-distance="<?php echo $packages[2]->distance * 1000; ?>">Pilih</button></td>
										<td class="bg-platinum"><button type="button" class="btn1 btn-white-fill btn-package" data-id="<?php echo $packages[3]->id; ?>" data-price="<?php echo $packages[3]->price; ?>" data-distance="<?php echo $packages[3]->distance * 1000; ?>">Pilih</button></td>
									</tr>
								</tbody>
							</table>
						</div>
						<div role="tabpanel" class="tab-pane" id="custom">
							<h3 align="center">Formulir Pemesanan Custom</h3>
							<table class="table">
								<thead>
									<tr>
										<th>Barang/Jasa</th>
										<th>Harga Satuan (Rp)</th>
										<th>Pemesanan (Qty)</th>
										<th>Subtotal</th>
									</tr>
								</thead>
								<tbody id="table-custom-price">
									<?php foreach ($prices as $index => $p): ?>
									<tr>
										<th><?php echo $p->name; ?></th>
										<td><?php echo number_format($p->price, 0, '', '.'); ?></td>
										<td>
											<input type="hidden" name="custom_id[]" value="<?php echo $p->id; ?>" />
											<input type="number" class="custom-qty" data-id="<?php echo $index+1; ?>" data-price="<?php echo $p->price; ?>" name="custom[]" value="0" min="0" max="20"></input></td>
										<td><span id="subtotal-<?php echo $index+1; ?>">-</span><input type="hidden" id="subtotal-hidden-<?php echo $index+1; ?>"></td>
									</tr>	
									<?php endforeach ?>
								</tbody>
							</table>
						</div>
					</div>
						
					<h4 align="center">Biaya utama pindahan anda: <span id="confirmation-price">-</span></h4 align="center">
					<div class="text-center">
						<input type="hidden" name="package_id" id="package_id" value=""></input>
						<input type="hidden" name="price" id="price" value="" required="">
					</div>
					<br><br>

				</section>

				<section id="dataperabot">

					<legend>Informasi Perabotan</legend>
					<div class="form-inline text-center">
						<div class="form-group">
							<label class="sr-only">Jenis Perabotan</label>
							<select class="form-control" id="appliances">
								<option value="">Jenis Perabotan</option>
								<!-- <option value="1">Ranjang</option>
								<option value="2">Lemari</option>
								<option value="3">Kulkas</option>
								<option value="4">Mesin Cuci</option>
								<option value="5">Televisi</option>
								<option value="6">Meja</option>
								<option value="7">Kursi</option> -->
								<?php foreach ($appliances as $a): ?>
								<option value="<?php echo $a->id ?>"><?php echo $a->name; ?></option>
								<?php endforeach ?>
							</select>
						</div>

						<div class="form-group" style="position:relative; margin-left: 50px">
							<label class="sr-only">Ukuran</label>
							<select class="form-control" id="sizes">
								<option value="">Ukuran</option>
							</select>
						</div>

						<button type="button" class="btn1 btn-blue" id="btn-add-appliance" style="position:relative; margin-left: 50px"><i class="fa fa-plus"></i> Tambah</button>
					</div>
					<br>
					<p id="no-appliances" class="text-center">Anda belum menambahkan perabotan.</p>
					<table id="table-appliances" class="table table-bordered table-hover" style="display: none;">
						<thead>
							<tr>
								<th style="width: 30%;">Jenis Perabotan</th>
								<th style="width: 25%">Ukuran</th>
								<th style="width: 15%">Jumlah</th>
								<th style="width: 5%">Action</th>
							</tr>
						</thead>
						<tbody id="appliance-row">
						</tbody>
					</table>

				</section>

				<div class="text-center">
					<br><br>
					<h5>Biaya transportasi anda (Rp <?php echo number_format($distance_price->price_per_km, 0, '', '.'); ?>,- per km): <span id="transportation-price">-</span></h5>
					<h3>Estimasi total harga pindahan anda: <span id="total-price">-</span></h3>
					<br><br>
					<button type="submit" class="btn1 btn-blue">Pesan</button>
					<button type="reset" class="btn1 btn-blue-fill">Reset</button>
				</div>

			</fieldset>
		</form>


	</div>

	<?php include('partials/footer.php'); ?>
	
	<script type="text/javascript">
	
	// get sizes for appliance
	$('#appliances').change(function() {
		$applianceID = $(this).val();
		$.ajax({
			url: '<?php echo site_url("order/get_size"); ?>/' + $applianceID,
			success: function(res) {
				if(res){
					$('#sizes').empty();
					$('#sizes').append('<option value="">Ukuran</option>');
					for (var i = 0; i < res.length; i++) {
						$('#sizes').append('<option value="' + res[i].id + '">' + res[i].name + ' (' + res[i].dimension + ')</option>');
					}
				}
			}
		});
	});
	var firstLoad = 0;

	// timepicker
    $('#timepicker2').datetimepicker({
	    format: 'LT',
	    locale: 'id'
	}).on('dp.hide', checkDatetime);

	$('#moving-date').on('changeDate', checkDatetime);

	// check moving date & time
	function checkDatetime() {
		if(firstLoad == 0){
			firstLoad = 1;
			return;
		}
		var datetime = $('#moving-date').val() + ' ' + $('#timepicker2').val();
		$.ajax({
			url: '<?php echo site_url("order/check_moving_time"); ?>',
			type: 'POST',
			data: {'datetime' : datetime},
			success: function(res) {
				if(!res.available){
					$('#timepicker2').val('');
					alert('Pilihan waktu anda tidak tersedia. \nNote: Harap tidak memilih di luar jam 07:00 - 20:00 WIB.');
				}
			}
		});
		$.ajax({
			url: '<?php echo site_url("order/get_order_by_date"); ?>',
			type: 'POST',
			data: {'date' : $('#moving-date').val()},
			success: function(res) {
				console.log(res);
			}
		});
	}
	</script>