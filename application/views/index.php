<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Pindahan | Solusi Pindahan Mudah</title>
	<meta name="description" content="Solusi pindahan mudah dan murah" />
	<meta name="keywords" content="pindahan, murah, mudah, rumah, kost" />
	<meta name="author" content="Vinsensius Danny" />
	<!-- Favicons (created with http://realfavicongenerator.net/)-->
	<link rel="apple-touch-icon" sizes="57x57" href="img/favicons/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="img/favicons/apple-touch-icon-60x60.png">
	<link rel="icon" type="image/png" href="img/favicons/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="img/favicons/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="img/favicons/manifest.json">
	<link rel="shortcut icon" href="img/favicons/favicon.ico">
	<meta name="msapplication-TileColor" content="#00a8ff">
	<meta name="msapplication-config" content="img/favicons/browserconfig.xml">
	<meta name="theme-color" content="#ffffff">
	<!-- Normalize -->
	<link rel="stylesheet" type="text/css" href="<?php echo asset_url();?>css/normalize.css">
	<!-- Bootstrap -->
	<link rel="stylesheet" type="text/css" href="<?php echo asset_url();?>css/bootstrap.css">
	<!-- Owl -->
	<link rel="stylesheet" type="text/css" href="<?php echo asset_url();?>css/owl.css">
	<!-- Animate.css -->
	<link rel="stylesheet" type="text/css" href="<?php echo asset_url();?>css/animate.css">
	<!-- Font Awesome -->
	<link rel="stylesheet" type="text/css" href="<?php echo asset_url();?>fonts/font-awesome-4.1.0/css/font-awesome.min.css">
	<!-- Elegant Icons -->
	<link rel="stylesheet" type="text/css" href="<?php echo asset_url();?>fonts/eleganticons/et-icons.css">
	<!-- Main style -->
	<link rel="stylesheet" type="text/css" href="<?php echo asset_url();?>css/cardio.css">
	<!-- Custom style -->
	<link rel="stylesheet" type="text/css" href="<?php echo asset_url();?>css/custom.css">
</head>

<body>
	<div class="preloader">
		<img src="<?php echo asset_url(); ?>img/loader.gif" alt="Preloader image">
	</div>
	
	<nav class="navbar">
		<div class="container">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#"><i class="fa fa-home fa-3x"></i></a>
			</div>
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right main-nav">
					<li><a href="#intro">Intro</a></li>
					<li><a href="#promotions">Promosi</a></li>
					<li><a href="#services">Layanan</a></li>
					<!-- <li><a href="#team">Tim Kami</a></li> -->
					<li><a href="#pricing">Harga</a></li>
					<?php if ($this->session->has_userdata('user') == FALSE): ?>
					<li>
						<a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							Member  <span class="caret"></span>
    					</a>
    					<ul class="dropdown-menu" aria-labelledby="dLabel">
    						<li><a href="<?php echo site_url('signup'); ?>">Daftar</a></li>
    						<li><a href="#" data-toggle="modal" data-target="#modal1">Masuk</a></li>
						</ul>
					</li>
					<?php else: ?>
					<li><a href="<?php echo site_url('signin/logout'); ?>">Keluar</a></li>
					<?php endif ?>
					<li><a href="<?php echo site_url('order') ?>" class="btn1 btn-blue">Pesan</a></li>
				</ul>
			</div>
			<!-- /.navbar-collapse -->
		</div>
		<!-- /.container-fluid -->
	</nav>
	<header id="intro">
		<div class="container">
			<div class="table">
				<div class="header-text">
					<div class="row">
						<div class="col-md-12 text-center"><!-- 
							<h3 class="light white">Ingin pindahan mudah dan murah?</h3>
							<h1 class="white typed">Coba Pindahan.com aja!</h1> -->
							<h3 class="light white">Layanan pindahan terpercaya untuk</h3>
							<h1 class="typed white">Kos &bull; Apartment &bull; Rumah &bull; Merchant</h1>
							<span class="typed-cursor">_</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	<section>
		<div class="cut cut-top"></div>
		<div class="container">
			<div class="row intro-tables">
				<div class="col-md-4">
					<div class="intro-table intro-table-hover intro-table-red">
						<h5 class="white heading hide-hover"></h5>
						<div class="bottom">
							<h4 class="white heading">Mengapa pakai Pindahan.com?</h4>
							<h4 class="white heading small-pt">&nbsp;</h4>
							<div class="heading author white expand">
								<li>
									Waktu pindahan yang fleksibel
								</li>
								<li>
									Harga pindahan yang terjangkau
								</li>
								<li>
									Pindahan dengan aman dan nyaman
								</li>
								<li>
									Pemesanan online yang praktis dan informatif
								</li>
								<li>
									Pekerja yang handal dan terampil
								</li>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="intro-table intro-table-hover">
						<h5 class="white heading hide-hover"></h5>
						<div class="bottom">
							<h4 class="white heading small-heading no-margin regular">Ayo pindah!</h4>
							<h4 class="white heading small-pt">mudah, murah dan aman</h4>
							<a href="#" class="btn1 btn-white-fill expand">Pindah sekarang</a>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="intro-table intro-table-third">
						
						<h5 class="white heading">Jadwal Pindahan</h5>
						<div class="owl-carousel owl-schedule bottom">
							<div class="item">
								<div class="schedule-row row">
									<div class="col-xs-6">
										<h5 class="regular white">Shift 1</h5>
									</div>
									<div class="col-xs-6 text-right">
										<h5 class="white">8:30 - 10:00</h5>
									</div>
								</div>
								<div class="schedule-row row">
									<div class="col-xs-6">
										<h5 class="regular white">Shift 2</h5>
									</div>
									<div class="col-xs-6 text-right">
										<h5 class="white">8:30 - 10:00</h5>
									</div>
								</div>
								<div class="schedule-row row">
									<div class="col-xs-6">
										<h5 class="regular white">Shift 3</h5>
									</div>
									<div class="col-xs-6 text-right">
										<h5 class="white">8:30 - 10:00</h5>
									</div>
								</div>
							</div>
							<div class="item">
								<div class="schedule-row row">
									<div class="col-xs-6">
										<h5 class="regular white">Shift 4</h5>
									</div>
									<div class="col-xs-6 text-right">
										<h5 class="white">8:30 - 10:00</h5>
									</div>
								</div>
								<div class="schedule-row row">
									<div class="col-xs-6">
										<h5 class="regular white">Shift 5</h5>
									</div>
									<div class="col-xs-6 text-right">
										<h5 class="white">8:30 - 10:00</h5>
									</div>
								</div>
								<div class="schedule-row row">
									<div class="col-xs-6">
										<h5 class="regular white">Shift 6</h5>
									</div>
									<div class="col-xs-6 text-right">
										<h5 class="white">8:30 - 10:00</h5>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section id="promotions" class="section section-padded">
		<div class="container">
			<div class="row text-center title">
				<h2>Promosi</h2>
				<h4 class="light muted">Dapatkan harga terbaik untuk mempermudah kebutuhan pindahan anda</h4>
				<br>
			</div>
			<div id="carousel-promotions" class="carousel slide" data-ride="carousel" style="width: 1000px; margin: 0 auto">
				<!-- Indicators -->
				<ol class="carousel-indicators">
					<li data-target="#carousel-promotions" data-slide-to="0" class="active"></li>
					<li data-target="#carousel-promotions" data-slide-to="1"></li>
					<li data-target="#carousel-promotions" data-slide-to="2"></li>
				</ol>
				<!-- Wrapper for slides -->
				<div class="carousel-inner" role="listbox">
					<div class="item active">
						<img src="<?php echo asset_url(); ?>img/promo/promo1.jpg" alt="...">
						<div class="carousel-caption">
						</div>
					</div>
					<div class="item">
						<img src="<?php echo asset_url(); ?>img/promo/promo2.jpg" alt="...">
						<div class="carousel-caption">
						</div>
					</div>
					<div class="item">
						<img src="<?php echo asset_url(); ?>img/promo/promo3.jpg" alt="...">
						<div class="carousel-caption">
						</div>
					</div>
				</div>
				<!-- Controls -->
				<a class="left carousel-control" href="#carousel-promotions" role="button" data-slide="prev">
					<span class="fa fa-chevron-left fa-2x carousel-nav" style="" aria-hidden="true"></span>
					<span class="sr-only">Previous</span>
				</a>
				<a class="right carousel-control" href="#carousel-promotions" role="button" data-slide="next">
					<span class="fa fa-chevron-right fa-2x carousel-nav" aria-hidden="true"></span>
					<span class="sr-only">Next</span>
				</a>
			</div>
		</div>
	</section>
	<section id="services" class="section section-padded">
		<div class="container">
			<div class="row text-center title">
				<h2>Layanan</h2>
				<h4 class="light muted">Pindahan dengan mudah dan nyaman dengan berbagai layanan yang kami sediakan</h4>
			</div>
			<div class="row services">
				<div class="col-md-4">
					<div class="service">
						<div class="icon-holder">
							<!-- <img src="img/icons/guru-blue.png" alt="" class="icon"> -->
							<i class="fa fa-truck fa-3x icon"></i>
						</div>
						<h4 class="heading">Transportasi</h4>
						<p class="description">Layanan transportasi menuju tempat baru yang Anda tuju.</p>
					</div>
				</div>
				<div class="col-md-4">
					<div class="service">
						<div class="icon-holder">
							<!-- <img src="img/icons/heart-blue.png" alt="" class="icon"> -->
							<i class="fa fa-cube fa-3x icon"></i>
						</div>
						<h4 class="heading">Packing</h4>
						<p class="description">Bantuan pengemasan barang-barang yang akan dipindahkan oleh tenaga professional.</p>
					</div>
				</div>
				<div class="col-md-4">
					<div class="service">
						<div class="icon-holder">
							<!-- <img src="img/icons/weight-blue.png" alt="" class="icon"> -->
							<i class="fa fa-dropbox fa-3x icon"></i>
						</div>
						<h4 class="heading">Unpacking + Penataan</h4>
						<p class="description">Bantuan dalam menata kembali barang-barang Anda di tempat yang baru.</p>
					</div>
				</div>
			</div>
		</div>
		<div class="cut cut-bottom"></div>
	</section>
	<!-- <section id="team" class="section gray-bg">
		<div class="container">
			<div class="row title text-center">
				<h2 class="margin-top">Tim Kami</h2>
				<h4 class="light muted">Pekerja terbaik untuk anda</h4>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="team text-center">
						<div class="cover" style="background:url('<?php echo asset_url(); ?>img/team/team-cover1.jpg'); background-size:cover;">
							<div class="overlay text-center">
								<h5 class="light white"><i>"Lorem ipsum dolor sit amet"</i></h5>
							</div>
						</div>
						<img src="<?php echo asset_url(); ?>img/team/team3.jpg" alt="Team Image" class="avatar">
						<div class="title">
							<h4>Ben Adamson</h4>
							<h5 class="muted regular">Fitness Instructor</h5>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="team text-center">
						<div class="cover" style="background:url('<?php echo asset_url(); ?>img/team/team-cover2.jpg'); background-size:cover;">
							<div class="overlay text-center">
								<h5 class="light white"><i>"Lorem ipsum dolor sit amet"</i></h5>
							</div>
						</div>
						<img src="<?php echo asset_url(); ?>img/team/team1.jpg" alt="Team Image" class="avatar">
						<div class="title">
							<h4>Eva Williams</h4>
							<h5 class="muted regular">Personal Trainer</h5>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="team text-center">
						<div class="cover" style="background:url('<?php echo asset_url(); ?>img/team/team-cover3.jpg'); background-size:cover;">
							<div class="overlay text-center">
								<h5 class="light white"><i>"Lorem ipsum dolor sit amet"</i></h5>
							</div>
						</div>
						<img src="<?php echo asset_url(); ?>img/team/team2.jpg" alt="Team Image" class="avatar">
						<div class="title">
							<h4>John Phillips</h4>
							<h5 class="muted regular">Personal Trainer</h5>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section> -->
	<section id="pricing" class="section">
		<div class="container">
			<div class="row title text-center">
				<h2 class="margin-top white">Harga</h2>
				<h4 class="light white">Pilihan dari berbagai paket dengan harga terbaik</h4>
			</div>
			<div class="row no-margin">
				<div class="col-md-8 no-padding col-md-offset-4 pricings text-center">
					<div class="pricing">
						<div class="box-main active bg-bronze" data-img="<?php echo asset_url(); ?>img/pricing1.jpg">
							<h4 class="white">Paket Bronze</h4>
							<h4 class="white regular light"><span class="small-font">harga mulai Rp</span><b>120.000</b></h4>
							<a href="<?php echo site_url('order'); ?>" class="btn1 btn-white-fill">Pesan sekarang</a>
							<i class="info-icon icon_question"></i>
						</div>
						<div class="box-second active bg-bronze">
							<ul class="white-list text-left">
								<li>1 mobil box + gratis 2 km pertama</li>
							</ul>
						</div>
					</div>
					<div class="pricing">
						<div class="box-main bg-silver" data-img="<?php echo asset_url(); ?>img/pricing2.jpg">
							<h4 class="white">Paket Silver</h4>
							<h4 class="white regular light"><span class="small-font">harga mulai Rp</span><b>200.000</b></h4>
							<a href="<?php echo site_url('order'); ?>" class="btn1 btn-white-fill">Pesan sekarang</a>
							<i class="info-icon icon_question"></i>
						</div>
						<div class="box-second bg-silver">
							<ul class="white-list text-left">
								<li>1 mobil box + gratis 2 km pertama</li>
								<li>2 tenaga angkat</li>
							</ul>
						</div>
					</div>
					<div class="pricing">
						<div class="box-main bg-gold" data-img="<?php echo asset_url(); ?>img/pricing2.jpg">
							<h4 class="white">Paket Gold</h4>
							<h4 class="white regular light"><span class="small-font">harga mulai Rp</span><b>350.000</b></h4>
							<a href="<?php echo site_url('order'); ?>" class="btn1 btn-white-fill">Pesan sekarang</a>
							<i class="info-icon icon_question"></i>
						</div>
						<div class="box-second bg-gold">
							<ul class="white-list text-left">
								<li>1 mobil box + gratis 5 km pertama</li>
								<li>3 tenaga angkat</li>
								<li>3 tenaga packing</li>
							</ul>
						</div>
					</div>
					<div class="pricing">
						<div class="box-main bg-platinum" data-img="<?php echo asset_url(); ?>img/pricing2.jpg">
							<h4 class="white">Paket Platinum</h4>
							<h4 class="white regular light"><span class="small-font">harga mulai Rp</span><b>600.000</b></h4>
							<a href="<?php echo site_url('order'); ?>" class="btn1 btn-white-fill">Pesan sekarang</a>
							<i class="info-icon icon_question"></i>
						</div>
						<div class="box-second bg-platinum">
							<ul class="white-list text-left">
								<li>1 mobil box + gratis 5 km pertama</li>
								<li>5 tenaga angkat</li>
								<li>5 tenaga packing</li>
								<li>5 tenaga unpacking + penataan</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="section section-padded blue-bg">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<div class="owl-twitter owl-carousel">
						<div class="item text-center">
							<i class="icon fa fa-home fa-3x"></i>
							<h4 class="white light">If everyone is moving forward together, then success takes care of itself.</h4>
							<h4 class="light-white light">- Henry Ford</h4>
						</div>
						<div class="item text-center">
							<i class="icon fa fa-home fa-3x"></i>
							<h4 class="white light">Life is like riding a bicycle. To keep your balance, you must keep moving.</h4>
							<h4 class="light-white light">- Albert Einstein</h4>
						</div>
						<div class="item text-center">
							<i class="icon fa fa-home fa-3x"></i>
							<h4 class="white light">Stay focused, go after your dreams and keep moving toward your goals.</h4>
							<h4 class="light-white light">-- LL Cool J</h4>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<?php include('modals/login.php'); ?>
	<?php include('partials/footer.php'); ?>
	
