<h2>
	Custom Services
	<a href="<?php echo site_url('backend/testimonial/add'); ?>" class="btn btn-sm btn-primary pull-right"><i class="glyphicon glyphicon-plus"></i> Add</a>
</h2>
<?php if(count($prices) > 0) {?>
<table class="table table-bordered table-striped table-hover">
	<thead>
		<tr>
			<th>Order ID</th>
			<th>Rate</th>
			<th>Comment</th>
			<th>Created at</th>
			<th>Updated at</th>
			<th>Actions</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($prices as $p) { ?>
			<tr>
				<td><?php echo $p->order_id; ?></td>
				<td><?php echo $p->rate; ?></td>
				<td><?php echo $p->comment; ?></td>
				<td><?php echo $p->created_at; ?></td>
				<td><?php echo $p->updated_at; ?></td>
				<td>
					<a href="<?php echo site_url('backend/testimonial/edit/' . $p->id); ?>" title="Edit"><i class="glyphicon glyphicon-edit"></i></a>&nbsp;
					<a href="<?php echo site_url('backend/testimonial/delete/' . $p->id); ?>" title="Delete" onclick="return confirm('Are you sure to delete this item?');"><i class="glyphicon glyphicon-trash"></i></a>
				</td>
			</tr>
		<?php }?>
	</tbody>
</table>
<?php 
}
else {?>
<h4>No data recorded yet!</h4>
<?php } ?>

