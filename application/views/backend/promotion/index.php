<h2>
	Promotions
	<a href="<?php echo site_url('backend/promotion/add'); ?>" class="btn btn-sm btn-primary pull-right"><i class="glyphicon glyphicon-plus"></i> Add</a>
</h2>
<?php if(count($promotions) > 0) {?>
<table class="table table-bordered table-striped table-hover">
	<thead>
		<tr>
			<th>Image</th>
			<th>Created at</th>
			<th>Updated at</th>
			<th>Last Modifier</th>
			<th>Actions</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($promotions as $p) { ?>
			<tr>
				<td><img src="<?php echo upload_url('promotions/' . $p->image_path); ?>" width="300"></td>
				<td><?php echo $p->created_at; ?></td>
				<td><?php echo $p->updated_at; ?></td>
				<td><?php echo $p->modified_by; ?></td>
				<td>
					<a href="<?php echo site_url('backend/promotion/edit/' . $p->id); ?>" title="Edit"><i class="glyphicon glyphicon-edit"></i></a>&nbsp;
					<a href="<?php echo site_url('backend/promotion/delete/' . $p->id); ?>" title="Delete" onclick="return confirm('Are you sure to delete this item?');"><i class="glyphicon glyphicon-trash"></i></a>
				</td>
			</tr>
		<?php }?>
	</tbody>
</table>
<?php 
}
else {?>
<h4>No data recorded yet!</h4>
<?php } ?>

