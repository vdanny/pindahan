<h2>Edit Promotion</h2>

<?php echo form_open_multipart('backend/promotion/doEdit', array('class' => 'form-horizontal'));?>
	<input type='hidden' name="id" value="<?php echo isset($promotion)? $promotion->id : set_value('id'); ?>"></input>
	<?php $this->load->view('backend/promotion/_form'); ?>

</form>