<fieldset>
	
	<!-- Text input-->
	<div class="form-group">
	    <label class="col-md-4 control-label" for="image">Image</label>
	    <div class="col-md-4">
	        <input id="image" name="image" placeholder="Select image" type="file">
	        
	    </div>
	</div>

	<div class="form-group">
		<div class="col-md-4 col-md-offset-4">
			<p class="help-block">Preview Image</p>
			<img id="preview" width="300" <?php echo isset($promotion)? 'src="' . upload_url('promotions/' . $promotion->image_path) . '"' : ''; ?>>
		</div>
	</div>

	<!-- Button -->
    <div class="form-group">
        <label class="col-md-4 control-label" for="btn-submit"></label>
        <div class="col-md-4">
            <button id="btn-submit" class="btn btn-primary">Submit</button>
            <a href="<?php echo site_url('backend/customservice/index'); ?>" id="btn-submit" name="btn-submit" class="btn btn-default">Back</a>
        </div>
    </div>

</fieldset>

<?php if($this->session->has_userdata('error')) {?>
<div class="alert alert-danger">
    <?php echo $this->session->flashdata('error'); ?>
</div>
<?php } ?>