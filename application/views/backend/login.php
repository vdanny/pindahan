<!DOCTYPE html>
<html>
<head>

	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

	<title>Login Pindahan Backend</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo asset_url();?>backend/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo asset_url();?>backend/css/simple-sidebar.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <style type="text/css">
	body {
		background-color:#fff;
		-webkit-font-smoothing: antialiased;
		font: normal 14px arial,sans-serif;
	}

	.container {
	    padding: 25px;
	    position: fixed;
	}

	.form-login {
	    background-color: #EDEDED;
	    padding-top: 10px;
	    padding-bottom: 20px;
	    padding-left: 20px;
	    padding-right: 20px;
	    border-radius: 15px;
	    border-color:#d2d2d2;
	    border-width: 5px;
	    box-shadow:0 1px 0 #cfcfcf;
	}

	h4 { 
		border:0 solid #fff; 
		border-bottom-width:1px;
		padding-bottom:10px;
		text-align: center;
	}

	.form-control {
	    border-radius: 10px;
	}

	.wrapper {
	    text-align: center;
	}

    </style>
</head>
<body>

	<div class="container">
	    <div class="row">
	        <div class="col-md-offset-5 col-md-3">
	        	<form method="POST" action="doLogin">
				<div class="form-login">
					<h4>Admin Login</h4>
					<input type="text" id="username" name="username" class="form-control input-sm chat-input" placeholder="Username" />
					</br>
					<input type="password" id="password" name="password" class="form-control input-sm chat-input" placeholder="Password" />
					</br>
					<div class="wrapper">
						<button type="submit" class="btn btn-default btn-md">Submit</button>
					</div>
				</div>
				</form>
				<br>
				<?php if(isset($_SESSION['msg'])) {?>	
					<div class="alert alert-danger" role="alert"><b>Error</b> <?php echo $this->session->flashdata('msg'); ?></div>
				<?php } ?>
	        </div>
	    </div>
	</div>


    <!-- jQuery -->
    <script src="<?php echo asset_url();?>backend/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo asset_url();?>backend/js/bootstrap.min.js"></script>
</body>
</html>