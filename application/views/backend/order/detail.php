<h2>
	Order Details for Order #<?php echo $order->id ?>
	<a href="<?php echo site_url('backend/order/add_detail/'.$order->id); ?>" class="btn btn-sm btn-primary pull-right"><i class="glyphicon glyphicon-plus"></i> Add</a>
	<a href="<?php echo site_url('backend/order/index') ?>" class="btn btn-sm btn-default pull-right">Back</a>
</h2>
<?php if(count($details) > 0) {?>
<table class="table table-bordered table-striped table-hover">
	<thead>
		<tr>
			<th>Name</th>
			<th>Quantity</th>
			<th>Created at</th>
			<th>Updated at</th>
			<th>Actions</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($details as $d) { ?>
			<tr>
				<td><?php echo $d->customprice->name; ?></td>
				<td><?php echo $d->qty; ?></td>
				<td><?php echo $d->created_at; ?></td>
				<td><?php echo $d->updated_at; ?></td>
				<td>
					<a href="<?php echo site_url('backend/order/edit_detail/' . $order->id . '/' . $d->id); ?>" title="Edit"><i class="glyphicon glyphicon-edit"></i></a>&nbsp;
					<a href="<?php echo site_url('backend/order/delete_detail/' . $order->id . '/' . $d->id); ?>" title="Delete" onclick="return confirm('Are you sure to delete this item?');"><i class="glyphicon glyphicon-trash"></i></a>
				</td>
			</tr>
		<?php }?>
	</tbody>
</table>
<?php 
}
else {?>
<h4>No data recorded yet!</h4>
<?php } ?>

