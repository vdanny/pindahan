<fieldset>
	
	<!-- Text input-->
	<div class="form-group">
	    <label class="col-md-4 control-label" for="user_id">UserID</label>
	    <div class="col-md-4">
	        <input id="user_id" name="user_id" placeholder="User ID" class="form-control input-md" type="text" maxlength="3" value="<?php echo isset($order)? $order->user_id : set_value('user_id'); ?>" readonly>
	        
	    </div>
	</div>

	<!-- Text input-->
	<div class="form-group">
	    <label class="col-md-4 control-label" for="fullname">Full name</label>
	    <div class="col-md-4">
	        <input id="fullname" name="fullname" placeholder="Full name" class="form-control input-md" type="text" maxlength="250" value="<?php echo isset($order)? $order->fullname : set_value('fullname'); ?>">
	        
	    </div>
	</div>

	<!-- Text input-->
	<div class="form-group">
	    <label class="col-md-4 control-label" for="phone">Phone</label>
	    <div class="col-md-4">
	        <input id="phone" name="phone" placeholder="Phone" class="form-control input-md" type="text" maxlength="20" value="<?php echo isset($order)? $order->phone : set_value('phone'); ?>">
	        
	    </div>
	</div>

	<!-- Text input-->
	<div class="form-group">
	    <label class="col-md-4 control-label" for="pickup_address">Pickup address</label>
	    <div class="col-md-4">
	        <textarea id="pickup_address" name="pickup_address" placeholder="Pickup address" class="form-control input-md" type="text" maxlength="500"><?php echo isset($order)? $order->pickup_address : set_value('pickup_address'); ?></textarea>
	        
	    </div>
	</div>
	<!-- Text input-->
	<div class="form-group">
	    <label class="col-md-4 control-label" for="destination_address">Destination address</label>
	    <div class="col-md-4">
	        <textarea id="destination_address" name="destination_address" placeholder="Destination address" class="form-control input-md" type="text" maxlength="500"><?php echo isset($order)? $order->destination_address : set_value('destination_address'); ?></textarea>
	        
	    </div>
	</div>

	<!-- Text input-->
	<div class="form-group">
	    <label class="col-md-4 control-label" for="package_id">Package</label>
	    <div class="col-md-4">
	        <select class="form-control" name="package_id">
	        	<?php foreach ($packages as $p): ?>
	        	<option value="<?php echo $p->id; ?>" <?php if(isset($order) && $order->package_id == $p->id) echo 'selected'; ?>><?php echo $p->name; ?></option>
	        	<?php endforeach ?>
	        	<option value="0">Custom</option>
	        </select>
	        
	    </div>
	</div>

	<!-- Text input-->
	<div class="form-group">
	    <label class="col-md-4 control-label" for="price">Price (IDR)</label>
	    <div class="col-md-4">
	        <input id="price" name="price" placeholder="Price" class="form-control input-md" type="number" min="0" value="<?php echo isset($order)? $order->price : set_value('price'); ?>">
	        
	    </div>
	</div>

	<!-- Text input-->
	<div class="form-group">
	    <label class="col-md-4 control-label" for="distance">Distance</label>
	    <div class="col-md-4">
	        <input id="distance" name="distance" placeholder="Distance" class="form-control input-md" type="number" min="0" value="<?php echo isset($order)? $order->distance : set_value('distance'); ?>">
	        
	    </div>
	</div>

	<!-- Text input-->
	<div class="form-group">
	    <label class="col-md-4 control-label" for="payment_status">Payment status</label>
	    <div class="col-md-4">
	        <input id="payment_status" name="payment_status" placeholder="Payment status" class="form-control input-md" type="text" maxlength="100" readonly=""
	        value="<?php if ($order->payment_status == 0) echo 'Unpaid'; 
	        			 else if($order->payment_status == 1) echo 'User confirmed';
						 else if($order->payment_status == 2) echo 'Paid';?>">
	        
	    </div>
	</div>

	<!-- Button -->
    <div class="form-group">
        <label class="col-md-4 control-label" for="btn-submit"></label>
        <div class="col-md-4">
            <button id="btn-submit" class="btn btn-primary">Submit</button>
            <a href="<?php echo site_url('backend/order/index'); ?>" id="btn-submit" name="btn-submit" class="btn btn-default">Back</a>
        </div>
    </div>

</fieldset>

<?php if($this->session->has_userdata('error')) {?>
<div class="alert alert-danger">
    <?php echo $this->session->flashdata('error'); ?>
</div>
<?php } ?>