<h2>
	Orders
	<!-- <a href="<?php echo site_url('backend/order/add'); ?>" class="btn btn-sm btn-primary pull-right"><i class="glyphicon glyphicon-plus"></i> Add</a> -->
</h2>
<?php if(count($orders) > 0) {?>
<table class="table table-bordered table-striped table-hover table-responsive">
	<thead>
		<tr>
			<th>Actions</th>
			<th>Status</th>
			<th>ID</th>
			<th>UserID</th>
			<th>Full name</th>
			<th>Phone</th>
			<th class="col-md-1 dont-break-out">Pickup Addr</th>
			<th class="col-md-1 dont-break-out">Destination Addr</th>
			<th>Moving Date/Time</th>
			<th>Package</th>
			<th>Price (IDR)</th>
			<th>Distance (meter)</th>
			<th>Created at</th>
			<th>Updated at</th>
			<th>Modified by</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($orders as $o) { ?>
			<tr>
				<td>
					<?php if ($o->package_id == 0): ?>
					<a href="<?php echo site_url('backend/order/detail/' . $o->id); ?>" title="Details"><i class="glyphicon glyphicon-list"></i></a>
					<?php endif ?>
					<a href="<?php echo site_url('backend/order/appliances/' . $o->id); ?>" title="Appliances"><i class="glyphicon glyphicon-lamp"></i></a>
					<a href="<?php echo site_url('backend/order/edit/' . $o->id); ?>" title="Edit"><i class="glyphicon glyphicon-edit"></i></a>&nbsp;
					<a href="<?php echo site_url('backend/order/delete/' . $o->id); ?>" title="Delete" onclick="return confirm('Are you sure to delete this item?');"><i class="glyphicon glyphicon-trash"></i></a>
				</td>
				<td>
					<?php if ($o->payment_status == 0): ?>
						Unpaid
					<?php elseif($o->payment_status == 1): ?>
						<a href="<?php echo site_url('backend/order/confirmation/' . $o->id) ?>">User confirmed</a>
					<?php elseif($o->payment_status == 2): ?>
						Paid
					<?php endif ?>
				</td>
				<td><?php echo $o->id ?></td>
				<td><?php echo $o->user_id; ?></td>
				<td><?php echo $o->fullname; ?></td>
				<td><?php echo $o->phone; ?></td>
				<td><?php echo $o->pickup_address; ?></td>
				<td><?php echo $o->destination_address; ?></td>
				<td><?php echo $o->moving_datetime; ?></td>
				<td><?php echo $o->package->name; ?></td>
				<td><?php echo number_format($o->price, 0, '', '.'); ?></td>
				<td><?php echo $o->distance; ?></td>
				<td><?php echo $o->created_at; ?></td>
				<td><?php echo $o->updated_at; ?></td>
				<td><?php echo $o->modified_by == ''? '-' : $o->modified_by; ?></td>
			</tr>
		<?php }?>
	</tbody>
</table>
<?php 
}
else {?>
<h4>No data recorded yet!</h4>
<?php } ?>

