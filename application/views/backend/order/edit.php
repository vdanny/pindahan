<h2>Edit Order</h2>

<form method="POST" action="<?php echo site_url('backend/order/doEdit'); ?>" class="form-horizontal">
	<input type='hidden' name="id" value="<?php echo isset($order)? $order->id : set_value('id'); ?>"></input>
	<?php $this->load->view('backend/order/_form'); ?>

</form>