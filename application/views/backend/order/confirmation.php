<h2>
	Confirmations for Order #<?php echo $order->id; ?>
</h2>

<table class="table table-bordered table-striped table-hover table-responsive">
	<thead>
		<tr>
			<th>ID</th>
			<th>UserID</th>
			<th>Full name</th>
			<th>Phone</th>
			<th class="col-md-1 dont-break-out">Pickup Addr</th>
			<th class="col-md-1 dont-break-out">Destination Addr</th>
			<th>Moving Date/Time</th>
			<th>Package</th>
			<th>Price (IDR)</th>
			<th>Distance (meter)</th>
			<th>Created at</th>
			<th>Updated at</th>
			<th>Modified by</th>
		</tr>
	</thead>
	<tbody>
			<tr>
				<td><?php echo $order->id ?></td>
				<td><?php echo $order->user_id; ?></td>
				<td><?php echo $order->fullname; ?></td>
				<td><?php echo $order->phone; ?></td>
				<td><?php echo $order->pickup_address; ?></td>
				<td><?php echo $order->destination_address; ?></td>
				<td><?php echo $order->moving_datetime; ?></td>
				<td><?php echo $order->package->name; ?></td>
				<td><?php echo number_format($order->price, 0, '', '.'); ?></td>
				<td><?php echo $order->distance; ?></td>
				<td><?php echo $order->created_at; ?></td>
				<td><?php echo $order->updated_at; ?></td>
				<td><?php echo $order->modified_by == ''? '-' : $order->modified_by; ?></td>
			</tr>
	</tbody>
</table>

<form class="form-horizontal" action="<?php echo site_url('backend/order/change_status/'.$order->id.'/2') ?>">
	<!-- Text input-->
	<div class="form-group">
	    <label class="col-md-4 control-label" for="bank_id">Transferred bank</label>
	    <div class="col-md-6">
	        <input id="bank_id" class="form-control input-md" type="text" maxlength="100" value="<?php echo $confirm->bank->bank_name.' - '.$confirm->bank->account_name.'('.$confirm->bank->account_no.')'; ?>" readonly>
	        
	    </div>
	</div>

	<!-- Text input-->
	<div class="form-group">
	    <label class="col-md-4 control-label" for="bank_name">Member's Bank</label>
	    <div class="col-md-4">
	        <input id="bank_name" class="form-control input-md" type="text" maxlength="100" value="<?php echo $confirm->bank_name; ?>" readonly>
	        
	    </div>
	</div>

	<!-- Text input-->
	<div class="form-group">
	    <label class="col-md-4 control-label" for="account_name">Member's Account Name</label>
	    <div class="col-md-4">
	        <input id="account_name" class="form-control input-md" type="text" maxlength="100" value="<?php echo $confirm->account_name; ?>" readonly>
	        
	    </div>
	</div>

	<!-- Text input-->
	<div class="form-group">
	    <label class="col-md-4 control-label" for="amount">Transferred Amount (IDR)</label>
	    <div class="col-md-4">
	        <input id="amount" class="form-control input-md" type="text" maxlength="100" value="<?php echo $confirm->amount; ?>" readonly>
	        
	    </div>
	</div>

	<!-- Text input-->
	<div class="form-group">
	    <label class="col-md-4 control-label" for="note">Note</label>
	    <div class="col-md-4">
	        <input id="note" class="form-control input-md" type="text" maxlength="100" value="<?php echo $confirm->note; ?>" readonly>
	        
	    </div>
	</div>

	<!-- Button -->
    <div class="form-group">
        <label class="col-md-4 control-label" for="btn-submit"></label>
        <div class="col-md-4">
            <button id="btn-submit" class="btn btn-primary">Confirm</button>
            <a href="<?php echo site_url('backend/order/index'); ?>" id="btn-submit" name="btn-submit" class="btn btn-default">Back</a>
        </div>
    </div>
</form>