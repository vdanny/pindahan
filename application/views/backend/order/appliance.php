<h2>
	Appliance Details for Order #<?php echo $order->id ?>
	<a href="<?php echo site_url('backend/order/add_appliance/'.$order->id); ?>" class="btn btn-sm btn-primary pull-right"><i class="glyphicon glyphicon-plus"></i> Add</a>
	<a href="<?php echo site_url('backend/order/index') ?>" class="btn btn-sm btn-default pull-right">Back</a>
</h2>
<?php if(count($appliances) > 0) {?>
<table class="table table-bordered table-striped table-hover">
	<thead>
		<tr>
			<th>Name</th>
			<th>Size</th>
			<th>Quantity</th>
			<th>Created at</th>
			<th>Updated at</th>
			<th>Actions</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($appliances as $d) { ?>
			<tr>
				<td><?php echo $d->appliance->name; ?></td>
				<td><?php echo $d->size->name; ?> <i>(<?php echo $d->size->dimension; ?>)</i></td>
				<td><?php echo $d->qty; ?></td>
				<td><?php echo $d->created_at; ?></td>
				<td><?php echo $d->updated_at; ?></td>
				<td>
					<a href="<?php echo site_url('backend/order/edit_appliance/' . $order->id . '/' . $d->id); ?>" title="Edit"><i class="glyphicon glyphicon-edit"></i></a>&nbsp;
					<a href="<?php echo site_url('backend/order/delete_appliance/' . $order->id . '/' . $d->id); ?>" title="Delete" onclick="return confirm('Are you sure to delete this item?');"><i class="glyphicon glyphicon-trash"></i></a>
				</td>
			</tr>
		<?php }?>
	</tbody>
</table>
<?php 
}
else {?>
<h4>No data recorded yet!</h4>
<?php } ?>

