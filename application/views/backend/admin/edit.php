<h2>Edit Administrator</h2>

<form method="POST" action="<?php echo site_url('backend/admin/doEdit'); ?>" class="form-horizontal">
	<input type='hidden' name="id" value="<?php echo isset($admin)? $admin->id : set_value('id'); ?>"></input>
	<?php $this->load->view('backend/admin/_form'); ?>

</form>