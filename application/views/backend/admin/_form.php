<fieldset>
    <!-- Text input-->
    <div class="form-group">
        <label class="col-md-4 control-label" for="username">Username</label>
        <div class="col-md-4">
            <input id="username" name="username" placeholder="Username" class="form-control input-md" required="" type="text" maxlength="100" value="<?php echo isset($admin)? $admin->username : set_value('username'); ?>">
            
        </div>
    </div>
    <!-- Password input-->
    <div class="form-group">
        <label class="col-md-4 control-label" for="password">Password</label>
        <div class="col-md-4">
            <input id="password" name="password" placeholder="Password" class="form-control input-md" type="password" maxlength="100">
            <?php if(isset($admin)){ ?>
            <p class="help-block">Skip this if you don't want to change password</p>
            <?php } ?>
        </div>
    </div>
    <!-- Password input-->
    <div class="form-group">
        <label class="col-md-4 control-label" for="confirm-password">Confirm Password</label>
        <div class="col-md-4">
            <input id="confirm-password" name="confirm-password" placeholder="Confirm Password" class="form-control input-md" type="password" maxlength="100">
            <?php if(isset($admin)){ ?>
            <p class="help-block">Skip this if you don't want to change password</p>
            <?php } ?>
        </div>
    </div>
    <!-- Multiple Checkboxes (inline) -->
    <div class="form-group">
        <label class="col-md-4 control-label" for="is_super">Super Admin</label>
        <div class="col-md-4">
            <label class="checkbox-inline" for="is_super">
                <input name="is_super" id="is_super" value="1" type="checkbox" 
                <?php   if(isset($admin) && $admin->is_super) echo 'checked'; 
                        else if(set_value('is_super')) echo 'checked';
                        else echo ''; ?>
                >
                Yes
            </label>
        </div>
    </div>
    
    <!-- Button -->
    <div class="form-group">
        <label class="col-md-4 control-label" for="btn-submit"></label>
        <div class="col-md-4">
            <button id="btn-submit" class="btn btn-primary">Submit</button>
            <a href="<?php echo site_url('backend/admin/index'); ?>" id="btn-submit" name="btn-submit" class="btn btn-default">Back</a>
        </div>
    </div>
</fieldset>

<?php if($this->session->has_userdata('error')) {?>
<div class="alert alert-danger">
    <?php echo $this->session->flashdata('error'); ?>
</div>
<?php } ?>