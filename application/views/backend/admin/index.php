<h2>
	Administrators
	<a href="<?php echo site_url('backend/admin/add'); ?>" class="btn btn-sm btn-primary pull-right"><i class="glyphicon glyphicon-plus"></i> Add</a>
</h2>
<?php if(count($admins) > 0) {?>
<table class="table table-bordered table-striped table-hover">
	<thead>
		<tr>
			<th>Username</th>
			<th>Password</th>
			<th>Super Admin</th>
			<th>Created at</th>
			<th>Updated at</th>
			<th>Actions</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($admins as $a) { ?>
			<tr>
				<td><?php echo $a->username; ?></td>
				<td><?php echo $a->password; ?></td>
				<td><?php echo $a->is_super? 'Yes' : 'No'; ?></td>
				<td><?php echo $a->created_at; ?></td>
				<td><?php echo $a->updated_at; ?></td>
				<td>
					<a href="<?php echo site_url('backend/admin/edit/' . $a->id); ?>" title="Edit"><i class="glyphicon glyphicon-edit"></i></a>&nbsp;
					<a href="<?php echo site_url('backend/admin/delete/' . $a->id); ?>" title="Delete" onclick="return confirm('Are you sure to delete this item?');"><i class="glyphicon glyphicon-trash"></i></a>
				</td>
			</tr>
		<?php }?>
	</tbody>
</table>
<?php 
}
else {?>
<h4>No data recorded yet!</h4>
<?php } ?>
