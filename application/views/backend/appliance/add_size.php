<h2>New Size for <?php echo $appliance->name; ?></h2>

<form method="POST" action="<?php echo site_url('backend/appliance/doCreateSize'); ?>" class="form-horizontal">
	<input type='hidden' name="appliance_id" value="<?php echo $appliance->id; ?>"></input>
	<?php $this->load->view('backend/appliance/_form_size'); ?>

</form>