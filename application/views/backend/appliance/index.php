<h2>
	Appliances
	<a href="<?php echo site_url('backend/appliance/add'); ?>" class="btn btn-sm btn-primary pull-right"><i class="glyphicon glyphicon-plus"></i> Add</a>
</h2>
<?php if(count($appliances) > 0) {?>
<table class="table table-bordered table-striped table-hover">
	<thead>
		<tr>
			<th>Name</th>
			<th>Created at</th>
			<th>Updated at</th>
			<th>Actions</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($appliances as $a) { ?>
			<tr>
				<td><?php echo $a->name; ?></td>
				<td><?php echo $a->created_at; ?></td>
				<td><?php echo $a->updated_at; ?></td>
				<td>
					<a href="<?php echo site_url('backend/appliance/size/' . $a->id); ?>" title="Sizes"><i class="glyphicon glyphicon-list"></i></a>
					<a href="<?php echo site_url('backend/appliance/edit/' . $a->id); ?>" title="Edit"><i class="glyphicon glyphicon-edit"></i></a>&nbsp;
					<a href="<?php echo site_url('backend/appliance/delete/' . $a->id); ?>" title="Delete" onclick="return confirm('Are you sure to delete this item?');"><i class="glyphicon glyphicon-trash"></i></a>
				</td>
			</tr>
		<?php }?>
	</tbody>
</table>
<?php 
}
else {?>
<h4>No data recorded yet!</h4>
<?php } ?>

