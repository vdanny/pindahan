<h2>Edit Appliance</h2>

<form method="POST" action="<?php echo site_url('backend/appliance/doEdit'); ?>" class="form-horizontal">
	<input type='hidden' name="id" value="<?php echo isset($appliance)? $appliance->id : set_value('id'); ?>"></input>
	<?php $this->load->view('backend/appliance/_form'); ?>

</form>