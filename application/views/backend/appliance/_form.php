<fieldset>
	
	<!-- Text input-->
	<div class="form-group">
	    <label class="col-md-4 control-label" for="name">Name</label>
	    <div class="col-md-4">
	        <input id="name" name="name" placeholder="Appliance name" class="form-control input-md" type="text" maxlength="100" required="" value="<?php echo isset($appliance)? $appliance->name : set_value('name'); ?>">
	        
	    </div>
	</div>

	<!-- Button -->
    <div class="form-group">
        <label class="col-md-4 control-label" for="btn-submit"></label>
        <div class="col-md-4">
            <button id="btn-submit" class="btn btn-primary">Submit</button>
            <a href="<?php echo site_url('backend/appliance/index'); ?>" id="btn-submit" name="btn-submit" class="btn btn-default">Back</a>
        </div>
    </div>

</fieldset>

<?php if($this->session->has_userdata('error')) {?>
<div class="alert alert-danger">
    <?php echo $this->session->flashdata('error'); ?>
</div>
<?php } ?>