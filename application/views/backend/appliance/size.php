<h2>
	Appliance Sizes for <i><?php echo $appliance->name; ?></i>
	<a href="<?php echo site_url('backend/appliance/add_size/' . $appliance->id); ?>" class="btn btn-sm btn-primary pull-right"><i class="glyphicon glyphicon-plus"></i> Add</a>
	<a href="<?php echo site_url('backend/appliance/index'); ?>" class="btn btn-sm btn-default pull-right"><i class="glyphicon glyphicon-triangle-left"></i> Back</a>
</h2>
<?php if(count($sizes) > 0) {?>
<table class="table table-bordered table-striped table-hover">
	<thead>
		<tr>
			<th>Name</th>
			<th>Dimension</th>
			<th>Created at</th>
			<th>Updated at</th>
			<th>Actions</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($sizes as $a) { ?>
			<tr>
				<td><?php echo $a->name; ?></td>
				<td><?php echo $a->dimension; ?></td>
				<td><?php echo $a->created_at; ?></td>
				<td><?php echo $a->updated_at; ?></td>
				<td>
					<a href="<?php echo site_url('backend/appliance/edit_size/' . $appliance->id . '/' . $a->id); ?>" title="Edit"><i class="glyphicon glyphicon-edit"></i></a>&nbsp;
					<a href="<?php echo site_url('backend/appliance/delete_size/' . $appliance->id . '/'  . $a->id); ?>" title="Delete" onclick="return confirm('Are you sure to delete this item?');"><i class="glyphicon glyphicon-trash"></i></a>
				</td>
			</tr>
		<?php }?>
	</tbody>
</table>
<?php 
}
else {?>
<h4>No data recorded yet!</h4>
<?php } ?>

