<h2>Edit Size</h2>

<form method="POST" action="<?php echo site_url('backend/appliance/doEditSize'); ?>" class="form-horizontal">
	<input type='hidden' name="appliance_id" value="<?php echo $appliance->id; ?>"></input>
	<input type='hidden' name="id" value="<?php echo isset($size)? $size->id : set_value('id'); ?>"></input>
	<?php $this->load->view('backend/appliance/_form_size'); ?>

</form>