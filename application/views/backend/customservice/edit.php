<h2>Edit Custom Price</h2>

<form method="POST" action="<?php echo site_url('backend/customservice/doEdit'); ?>" class="form-horizontal">
	<input type='hidden' name="id" value="<?php echo isset($price)? $price->id : set_value('id'); ?>"></input>
	<?php $this->load->view('backend/customservice/_form'); ?>

</form>