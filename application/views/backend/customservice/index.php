<h2>
	Custom Services
	<a href="<?php echo site_url('backend/customservice/add'); ?>" class="btn btn-sm btn-primary pull-right"><i class="glyphicon glyphicon-plus"></i> Add</a>
</h2>
<?php if(count($prices) > 0) {?>
<table class="table table-bordered table-striped table-hover">
	<thead>
		<tr>
			<th>Name</th>
			<th>Price</th>
			<th>Created at</th>
			<th>Updated at</th>
			<th>Last Modifier</th>
			<th>Actions</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($prices as $p) { ?>
			<tr>
				<td><?php echo $p->name; ?></td>
				<td><?php echo $p->price; ?></td>
				<td><?php echo $p->created_at; ?></td>
				<td><?php echo $p->updated_at; ?></td>
				<td><?php echo $p->modified_by; ?></td>
				<td>
					<a href="<?php echo site_url('backend/customservice/edit/' . $p->id); ?>" title="Edit"><i class="glyphicon glyphicon-edit"></i></a>&nbsp;
					<a href="<?php echo site_url('backend/customservice/delete/' . $p->id); ?>" title="Delete" onclick="return confirm('Are you sure to delete this item?');"><i class="glyphicon glyphicon-trash"></i></a>
				</td>
			</tr>
		<?php }?>
	</tbody>
</table>
<?php 
}
else {?>
<h4>No data recorded yet!</h4>
<?php } ?>

