<fieldset>
	
	<!-- Text input-->
	<div class="form-group">
	    <label class="col-md-4 control-label" for="name">Name</label>
	    <div class="col-md-4">
	        <input id="name" name="name" placeholder="Name" class="form-control input-md" type="text" maxlength="100" value="<?php echo isset($price)? $price->name : set_value('name'); ?>">
	        
	    </div>
	</div>

	<!-- Text input-->
	<div class="form-group">
	    <label class="col-md-4 control-label" for="price">Price</label>
	    <div class="col-md-4">
	        <input id="price" name="price" class="form-control input-md" type="number" min="1" max="100000000" value="<?php echo isset($price)? $price->price : set_value('price'); ?>">
	        
	    </div>
	</div>

    <!-- Button -->
    <div class="form-group">
        <label class="col-md-4 control-label" for="btn-submit"></label>
        <div class="col-md-4">
            <button id="btn-submit" class="btn btn-primary">Submit</button>
            <a href="<?php echo site_url('backend/customservice/index'); ?>" id="btn-submit" name="btn-submit" class="btn btn-default">Back</a>
        </div>
    </div>

</fieldset>

<?php if($this->session->has_userdata('error')) {?>
<div class="alert alert-danger">
    <?php echo $this->session->flashdata('error'); ?>
</div>
<?php } ?>