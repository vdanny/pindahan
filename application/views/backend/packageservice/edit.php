<h2>Edit Package</h2>

<form method="POST" action="<?php echo site_url('backend/packageservice/doEdit'); ?>" class="form-horizontal">
	<input type='hidden' name="id" value="<?php echo isset($package)? $package->id : set_value('id'); ?>"></input>
	<?php $this->load->view('backend/packageservice/_form'); ?>

</form>