<fieldset>
	
	<!-- Text input-->
	<div class="form-group">
	    <label class="col-md-4 control-label" for="name">Package Name</label>
	    <div class="col-md-4">
	        <input id="name" name="name" placeholder="Package Name" class="form-control input-md" type="text" maxlength="100" value="<?php echo isset($package)? $package->name : set_value('name'); ?>">
	        
	    </div>
	</div>

	<!-- Text input-->
	<div class="form-group">
	    <label class="col-md-4 control-label" for="transportation">Transportation</label>
	    <div class="col-md-4">
	        <input id="transportation" name="transportation" class="form-control input-md" type="number" max="10" value="<?php echo isset($package)? $package->transportation : set_value('transportation'); ?>">
	        <p class="help-block">Transportation amount provided</p>
	    </div>
	</div>

	<!-- Text input-->
	<div class="form-group">
	    <label class="col-md-4 control-label" for="distance">Free Distance</label>
	    <div class="col-md-4">
	        <input id="distance" name="distance" class="form-control input-md" type="number" max="10" value="<?php echo isset($package)? $package->distance : set_value('distance'); ?>">
	        <p class="help-block">Free distance provided (in kilometers)</p>
	    </div>
	</div>

	<!-- Text input-->
	<div class="form-group">
	    <label class="col-md-4 control-label" for="move_emp">Mover Employee</label>
	    <div class="col-md-4">
	        <input id="move_emp" name="move_emp" class="form-control input-md" type="number" max="10" value="<?php echo isset($package)? $package->move_emp : set_value('move_emp'); ?>">
	        <p class="help-block">Provided mover employee amount</p>
	    </div>
	</div>

	<!-- Text input-->
	<div class="form-group">
	    <label class="col-md-4 control-label" for="pack_emp">Packer Employee</label>
	    <div class="col-md-4">
	        <input id="pack_emp" name="pack_emp" class="form-control input-md" type="number" max="10" value="<?php echo isset($package)? $package->pack_emp : set_value('pack_emp'); ?>">
	        <p class="help-block">Provided packer employee amount</p>
	    </div>
	</div>

	<!-- Text input-->
	<div class="form-group">
	    <label class="col-md-4 control-label" for="unpack_emp">Unpacker Employee</label>
	    <div class="col-md-4">
	        <input id="unpack_emp" name="unpack_emp" class="form-control input-md" type="number" max="10" value="<?php echo isset($package)? $package->unpack_emp : set_value('unpack_emp'); ?>">
	        <p class="help-block">Provided unpacker employee amount</p>
	    </div>
	</div>

	<!-- Text input-->
	<div class="form-group">
	    <label class="col-md-4 control-label" for="description">Description</label>
	    <div class="col-md-4">
	        <textarea id="description" name="description" placeholder="Description" class="form-control input-md" type="text" max="500"><?php echo isset($package)? $package->description : set_value('description'); ?></textarea>
	        
	    </div>
	</div>

	<!-- Text input-->
	<div class="form-group">
	    <label class="col-md-4 control-label" for="price">Price</label>
	    <div class="col-md-4">
	        <input id="price" name="price" placeholder="Price" class="form-control input-md" type="number" min="1" max="1000000000" value="<?php echo isset($package)? $package->price : set_value('price'); ?>">
	        
	    </div>
	</div>
	
    <!-- Button -->
    <div class="form-group">
        <label class="col-md-4 control-label" for="btn-submit"></label>
        <div class="col-md-4">
            <button id="btn-submit" class="btn btn-primary">Submit</button>
            <a href="<?php echo site_url('backend/packageservice/index'); ?>" id="btn-submit" name="btn-submit" class="btn btn-default">Back</a>
        </div>
    </div>

</fieldset>

<?php if($this->session->has_userdata('error')) {?>
<div class="alert alert-danger">
    <?php echo $this->session->flashdata('error'); ?>
</div>
<?php } ?>