<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Pindahan Backend</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo asset_url();?>backend/css/bootstrap.min.css" rel="stylesheet">

    <!-- Bootstrap Datepicker -->
    <link rel="stylesheet" href="<?php echo asset_url();?>plugins/datepicker/css/bootstrap-datepicker3.min.css">

    <!-- Custom CSS -->
    <link href="<?php echo asset_url();?>backend/css/simple-sidebar.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav">
                <li class="sidebar-brand" style="background: rgb(44, 46, 48) none repeat scroll 0% 0%">
                    <a href="<?php echo site_url('backend/index');?>">
                        Pindahan Backend
                    </a>
                </li>
                <li>
                    <a href="<?php echo site_url('backend/index');?>">Dashboard</a>
                </li>
                <li>
                    <a href="<?php echo site_url('backend/order/index'); ?>">Orders</a>
                </li>
                <li>
                    <a href="<?php echo site_url('backend/packageservice/index'); ?>">Package Services</a>
                </li>
                <li>
                    <a href="<?php echo site_url('backend/customservice/index'); ?>">Custom Services</a>
                </li>
                <li>
                    <a href="<?php echo site_url('backend/member/index'); ?>">Members</a>
                </li>
                <?php if($this->session->has_userdata('admin') && $this->session->userdata('admin')->is_super){?>
                <li>
                    <a href="<?php echo site_url('backend/admin/index');?>">Administrators</a>
                </li>
                <?php }?>
                <li>
                    <a href="<?php echo site_url('backend/appliance/index'); ?>">Appliances</a>
                </li>
                <li>
                    <a href="#">Testimonials</a>
                </li>
                <li>
                    <a href="<?php echo site_url('backend/promotion/index'); ?>">Promotions</a>
                </li>
                <li>
                    <a href="#">Employees</a>
                </li>
                <li>
                    <a href="#">Employee Schedules</a>
                </li>
                <li>
                    <a href="<?php echo site_url('backend/logout');?>">Logout</a>
                </li>

            </ul>
        </div>
        <!-- /#sidebar-wrapper -->

        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <a href="#menu-toggle" class="btn btn-default" id="menu-toggle"><i class="glyphicon glyphicon-chevron-left"></i> Close sidebar</a>
                        <?php $this->load->view($content); ?>
                    </div>
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="<?php echo asset_url();?>backend/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo asset_url();?>backend/js/bootstrap.min.js"></script>

    <!-- Bootstrap Datepicker -->
    <script src="<?php echo asset_url();?>plugins/datepicker/js/bootstrap-datepicker.min.js"></script>
    <script src="<?php echo asset_url();?>plugins/datepicker/locales/bootstrap-datepicker.id.min.js"></script>

    <!-- Menu Toggle Script -->
    <script>
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
        if(this.innerHTML.indexOf('left') >= 0) this.innerHTML = '<i class="glyphicon glyphicon-chevron-right"></i> Open sidebar';
        else this.innerHTML = '<i class="glyphicon glyphicon-chevron-left"></i> Close sidebar';
    });
    
    // datepicker
    $('.datepicker').datepicker();

    <?php if($this->session->has_userdata('msg')) { ?>
    alert("<?php echo $this->session->flashdata('msg'); ?>");
    <?php } ?>

    
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#preview').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $('#image').change(function() {
        readURL(this);
    });
    </script>

</body>

</html>
