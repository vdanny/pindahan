<fieldset>
	
	<!-- Text input-->
	<div class="form-group">
	    <label class="col-md-4 control-label" for="email">Email Address</label>
	    <div class="col-md-4">
	        <input id="email" name="email" placeholder="Email Address" class="form-control input-md" type="email" maxlength="100" value="<?php echo isset($member)? $member->email : set_value('email'); ?>">
	        
	    </div>
	</div>

	<!-- Text input-->
	<div class="form-group">
	    <label class="col-md-4 control-label" for="password">Password</label>
	    <div class="col-md-4">
	        <input id="password" name="password" placeholder="Password" class="form-control input-md" type="password" maxlength="100">
	        
	    </div>
	</div>

	<!-- Text input-->
	<div class="form-group">
	    <label class="col-md-4 control-label" for="confirm-password">Confirm Password</label>
	    <div class="col-md-4">
	        <input id="confirm-password" name="confirm-password" placeholder="Confirm password" class="form-control input-md" type="password" maxlength="100">
	        
	    </div>
	</div>

	<!-- Text input-->
	<div class="form-group">
	    <label class="col-md-4 control-label" for="fullname">Full Name</label>
	    <div class="col-md-4">
	        <input id="fullname" name="fullname" placeholder="Full Name" class="form-control input-md" type="text" maxlength="100" value="<?php echo isset($member)? $member->fullname : set_value('fullname'); ?>">
	        
	    </div>
	</div>

	<!-- Select Basic -->
	<div class="form-group">
		<label class="col-md-4 control-label" for="gender">Gender</label>
		<div class="col-md-4">
			<select id="gender" name="gender" class="form-control">
				<option value="1" <?php echo isset($member) && $member->gender === 1? 'checked' : ''; ?>>Male</option>
				<option value="2" <?php echo isset($member) && $member->gender === 2? 'checked' : ''; ?>>Female</option>
			</select>
		</div>
	</div>

	<!-- Text input-->
	<div class="form-group">
	    <label class="col-md-4 control-label" for="dob">Birthdate</label>
	    <div class="col-md-4">
	        <input id="dob" name="dob" placeholder="Birthdate" class="form-control input-md datepicker" type="text" maxlength="100" readonly="" value="<?php 
	        if(isset($member)){
		        $dob = new DateTime($member->dob);
		        echo $dob->format('m/d/Y');
		    }
		    else{
		        echo set_value('dob'); 
		    }?>">
	        
	    </div>
	</div>

	<!-- Text input-->
	<div class="form-group">
	    <label class="col-md-4 control-label" for="phone">Phone No.</label>
	    <div class="col-md-4">
	        <input id="phone" name="phone" placeholder="Phone Number" class="form-control input-md" type="text" maxlength="100" value="<?php echo isset($member)? $member->phone : set_value('phone'); ?>">
	        
	    </div>
	</div>

	<!-- Text input-->
	<div class="form-group">
	    <label class="col-md-4 control-label" for="address">Address</label>
	    <div class="col-md-4">
	        <textarea id="address" name="address" placeholder="Address" class="form-control input-md" maxlength="100"><?php echo isset($member)? $member->address : set_value('address'); ?></textarea>
	    </div>
	</div>

    <!-- Multiple Checkboxes (inline) -->
    <div class="form-group">
        <label class="col-md-4 control-label" for="is_merchant">Merchant</label>
        <div class="col-md-4">
            <label class="checkbox-inline" for="is_merchant">
                <input name="is_merchant" id="is_merchant" value="1" type="checkbox" 
                <?php   if(isset($member) && $member->is_merchant) echo 'checked'; 
                        else if(set_value('is_merchant')) echo 'checked';
                        else echo ''; ?>
                >
                Yes
            </label>
        </div>
    </div>

    <!-- Text input-->
    <div class="form-group">
        <label class="col-md-4 control-label" for="company_name">Company Name</label>
        <div class="col-md-4">
            <input id="company_name" name="company_name" placeholder="Company Name" class="form-control input-md" type="text" maxlength="100" value="<?php echo isset($member)? $member->company_name : set_value('company_name'); ?>">
            
        </div>
    </div>

    <!-- Button -->
    <div class="form-group">
        <label class="col-md-4 control-label" for="btn-submit"></label>
        <div class="col-md-4">
            <button id="btn-submit" class="btn btn-primary">Submit</button>
            <a href="<?php echo site_url('backend/admin/index'); ?>" id="btn-submit" name="btn-submit" class="btn btn-default">Back</a>
        </div>
    </div>
</fieldset>

<?php if($this->session->has_userdata('error')) {?>
<div class="alert alert-danger">
    <?php echo $this->session->flashdata('error'); ?>
</div>
<?php } ?>