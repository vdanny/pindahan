<h2>
	Members
	<a href="<?php echo site_url('backend/member/add'); ?>" class="btn btn-sm btn-primary pull-right"><i class="glyphicon glyphicon-plus"></i> Add</a>
</h2>
<?php if(count($members) > 0) {?>
<table class="table table-bordered table-striped table-hover">
	<thead>
		<tr>
			<th>Email</th>
			<th>Fullname</th>
			<th>Gender</th>
			<th>Birthdate</th>
			<th>Phone</th>
			<th>Address</th>
			<th>Merchant</th>
			<th>Company</th>
			<th>Created at</th>
			<th>Updated at</th>
			<th>Actions</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($members as $m) { ?>
			<tr>
				<td><?php echo $m->email; ?></td>
				<td><?php echo $m->fullname; ?></td>
				<td><?php echo $m->gender == 1? 'Male' : 'Female'; ?></td>
				<td><?php echo $m->dob; ?></td>
				<td><?php echo $m->phone; ?></td>
				<td><?php echo $m->address; ?></td>
				<td><?php echo $m->is_merchant? 'Yes' : 'No'; ?></td>
				<td><?php echo $m->company_name; ?></td>
				<td><?php echo $m->created_at; ?></td>
				<td><?php echo $m->updated_at; ?></td>
				<td>
					<a href="<?php echo site_url('backend/member/edit/' . $m->id); ?>" title="Edit"><i class="glyphicon glyphicon-edit"></i></a>&nbsp;
					<a href="<?php echo site_url('backend/member/delete/' . $m->id); ?>" title="Delete" onclick="return confirm('Are you sure to delete this item?');"><i class="glyphicon glyphicon-trash"></i></a>
				</td>
			</tr>
		<?php }?>
	</tbody>
</table>
<?php 
}
else {?>
<h4>No data recorded yet!</h4>
<?php } ?>
