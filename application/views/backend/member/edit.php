<h2>Edit Member</h2>

<form method="POST" action="<?php echo site_url('backend/member/doEdit'); ?>" class="form-horizontal">
	<input type='hidden' name="id" value="<?php echo isset($member)? $member->id : set_value('id'); ?>"></input>
	<?php $this->load->view('backend/member/_form'); ?>

</form>