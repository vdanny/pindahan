<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Pindahan | Solusi Pindahan Mudah</title>
	<meta name="description" content="Solusi pindahan mudah dan murah" />
	<meta name="keywords" content="pindahan, murah, mudah, rumah, kost" />
	<meta name="author" content="Vinsensius Danny" />
	<!-- Favicons (created with http://realfavicongenerator.net/)-->
	<link rel="apple-touch-icon" sizes="57x57" href="img/favicons/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="img/favicons/apple-touch-icon-60x60.png">
	<link rel="icon" type="image/png" href="img/favicons/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="img/favicons/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="img/favicons/manifest.json">
	<link rel="shortcut icon" href="img/favicons/favicon.ico">
	<meta name="msapplication-TileColor" content="#00a8ff">
	<meta name="msapplication-config" content="img/favicons/browserconfig.xml">
	<meta name="theme-color" content="#ffffff">
	<!-- Normalize -->
	<link rel="stylesheet" type="text/css" href="<?php echo asset_url();?>css/normalize.css">
	<!-- Bootstrap -->
	<link rel="stylesheet" type="text/css" href="<?php echo asset_url();?>css/bootstrap.css">
	<!-- Owl -->
	<link rel="stylesheet" type="text/css" href="<?php echo asset_url();?>css/owl.css">
	<!-- Animate.css -->
	<link rel="stylesheet" type="text/css" href="<?php echo asset_url();?>css/animate.css">
	<!-- Font Awesome -->
	<link rel="stylesheet" type="text/css" href="<?php echo asset_url();?>fonts/font-awesome-4.1.0/css/font-awesome.min.css">
	<!-- Elegant Icons -->
	<link rel="stylesheet" type="text/css" href="<?php echo asset_url();?>fonts/eleganticons/et-icons.css">
	<!-- Main style -->
	<link rel="stylesheet" type="text/css" href="<?php echo asset_url();?>css/cardio.css">
	<!-- Custom style -->
	<link rel="stylesheet" type="text/css" href="<?php echo asset_url();?>css/custom.css">
	<!-- Bootstrap Datepicker -->
	<link rel="stylesheet" href="<?php echo asset_url();?>plugins/datepicker/css/bootstrap-datepicker3.min.css">
	<!-- Bootstrap Timepicker -->
	<link rel="stylesheet" href="<?php echo asset_url();?>plugins/datetimepicker/css/bootstrap-datetimepicker.min.css">

	<style type="text/css">
	.form-control {
		border: 1px solid #a0a0a0 !important;
		font-size: 15px !important;
	}
	.control-label {
		font-size: 15px;
	}
	.table-package > thead > tr > th {
		border: 2px solid #f9f9f9;
		color: white;
		height: auto;
		text-align: center;
		font-size: 20px;
	}
	.table-package > tbody > tr > td {
		border-top: 1px solid #f9f9f9;
		color: white;
		height: auto;
		text-align: center;
		font-size: 17px;
	}
	legend { 
		text-align: center;
	}
	form {
		margin-top: 150px;
		margin-bottom: 150px;
	}
	</style>
</head>

<body>
	<div class="preloader">
		<img src="<?php echo asset_url(); ?>img/loader.gif" alt="Preloader image">
	</div>

	<div class="navbar">
		<div class="container">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header" style="color: #00a8ff !important;">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="<?php echo site_url(''); ?>"><i class="fa fa-home fa-3x"></i></a>
			</div>
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" style="color: #00a8ff !important;">
				<ul class="nav navbar-nav navbar-right main-nav">
					<?php if($this->session->has_userdata('user') == FALSE) {?>
					<li><a href="<?php echo site_url('signup'); ?>">Daftar Member</a></li>
					<li><a href="<?php echo site_url('signin'); ?>">Masuk</a></li>
					<?php } else {?>
					<!-- <li><a href="<?php echo site_url('dashboard/index'); ?>">Dashboard</a></li> -->
					<li><a href="<?php echo site_url('myorder/index'); ?>">Order</a></li>
					<li><a href="<?php echo site_url('profile/index') ?>">Profil</a></li>
					<li><a href="<?php echo site_url('signin/logout') ?>">Keluar</a></li>
					<?php } ?>
				</ul>
			</div>
			<!-- /.navbar-collapse -->
		</div>
		<!-- /.container-fluid -->
	</div>