	<footer>
		<div class="container">
			<?php if ($this->session->has_userdata('user') == FALSE): ?>
			<div class="row">
				<div class="col-sm-6 text-center-mobile">
					<h3 class="white">Jadilah anggota sekarang!</h3>
					<h5 class="light regular light-white">Dapatkan berbagai keuntungan dan bonus.</h5>
					<a href="<?php echo site_url('signup'); ?>" class="btn1 btn-blue ripple trial-button">Daftar Sekarang</a>
				</div>
				<div class="col-sm-6 text-center-mobile">
					<h3 class="white">Jam Kerja <span class="open-blink"></span></h3>
					<div class="row opening-hours">
						<div class="col-sm-6 text-center-mobile">
							<h5 class="light-white light">Senin - Jumat</h5>
							<h3 class="regular white">07:00 - 24:00</h3>
						</div>
						<div class="col-sm-6 text-center-mobile">
							<h5 class="light-white light">Sabtu - Minggu</h5>
							<h3 class="regular white">10:00 - 24:00</h3>
						</div>
					</div>
				</div>
			</div>
			<?php endif ?>
			<div class="row bottom-footer text-center-mobile">
				<div class="col-sm-8">
					<p>&copy; 2015 All Rights Reserved. Developed by Pindahan.</p>
				</div>
				<div class="col-sm-4 text-right text-center-mobile">
					<ul class="social-footer">
						<li><a href="http://www.facebook.com/pages/Codrops/159107397912"><i class="fa fa-facebook"></i></a></li>
						<li><a href="http://www.twitter.com/codrops"><i class="fa fa-twitter"></i></a></li>
						<li><a href="https://plus.google.com/101095823814290637419"><i class="fa fa-google-plus"></i></a></li>
					</ul>
				</div>
			</div>
		</div>
	</footer>
	<!-- Holder for mobile navigation -->
	<div class="mobile-nav">
		<ul>
		</ul>
		<a href="#" class="close-link"><i class="arrow_up"></i></a>
	</div>
	<!-- Scripts -->
	<script src="<?php echo asset_url();?>js/jquery-1.11.1.min.js"></script>
	<script src="<?php echo asset_url();?>js/owl.carousel.min.js"></script>
	<script src="<?php echo asset_url();?>js/bootstrap.min.js"></script>
	<script src="<?php echo asset_url();?>js/wow.min.js"></script>
	<script src="<?php echo asset_url();?>js/typewriter.js"></script>
	<script src="<?php echo asset_url();?>js/jquery.onepagenav.js"></script>
	<script src="<?php echo asset_url();?>js/jquery.validate/jquery.validate.min.js"></script>
	<script src="<?php echo asset_url();?>js/jquery.validate/localization/messages_id.min.js"></script>
	<script src="<?php echo asset_url();?>plugins/datepicker/js/bootstrap-datepicker.min.js"></script>
	<script src="<?php echo asset_url();?>plugins/datepicker/locales/bootstrap-datepicker.id.min.js"></script>
	<script src="<?php echo asset_url();?>plugins/moment/moment.js"></script>
	<script src="<?php echo asset_url();?>plugins/moment/moment-with-locales.js"></script>
	<script src="<?php echo asset_url();?>plugins/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
	<script src="<?php echo asset_url();?>js/main.js"></script>
	<script src="<?php echo asset_url();?>js/custom.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBBX9fsSNoG8GGq2lPmgw7Vw8ZU0hkjTGU&libraries=places&callback=initMap"
        async defer></script>

	<script type="text/javascript">
		
    <?php if($this->session->has_userdata('msg')) { ?>
    alert("<?php echo $this->session->flashdata('msg'); ?>");
    <?php } ?>
    
	</script>
</body>

</html>