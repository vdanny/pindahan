	<?php include('/../partials/header.php') ?>

	<div class="container" style="padding-top: 130px; min-height: 600px;">
		
		<form class="form-horizontal" id="form-signup" method="POST" action="<?php echo site_url('profile/doEdit'); ?>">
			<fieldset>
				<!-- Form Name -->
				<h2 align="center">Profil</h2><br><br><br>
				
				<input type="hidden" name="id" value="<?php echo $user->id; ?>">
				<!-- Text input-->
				<div class="form-group">
					<label class="col-md-4 control-label" for="email">Email</label>
					<div class="col-md-5">
						<input id="email" name="email" placeholder="Alamat email aktif anda" class="form-control input-md" type="email" required="" value="<?php echo $user->email; ?>" readonly>
					</div>
				</div>
				<!-- Password input-->
				<div class="form-group">
					<label class="col-md-4 control-label" for="password">Password</label>
					<div class="col-md-4">
						<input id="password" name="password" placeholder="Password akun anda" class="form-control input-md" type="password">
						<p class="help-block">Kosongkan jika tidak ingin mengubah password</p>
					</div>
				</div>
				<!-- Password input-->
				<div class="form-group">
					<label class="col-md-4 control-label" for="confirm-password">Konfirmasi Password</label>
					<div class="col-md-4">
						<input id="confirm-password" name="confirm-password" placeholder="Konfirmasi password sebelumnya" class="form-control input-md" type="password">
						<p class="help-block">Kosongkan jika tidak ingin mengubah password</p>
					</div>
				</div>
				<!-- Text input-->
				<div class="form-group">
					<label class="col-md-4 control-label" for="fullname">Nama Lengkap</label>
					<div class="col-md-5">
						<input id="fullname" name="fullname" placeholder="Nama lengkap anda" class="form-control input-md" required="" type="text" value="<?php echo $user->fullname; ?>">
					</div>
				</div>
				<!-- Select Basic -->
				<div class="form-group">
					<label class="col-md-4 control-label" for="gender">Jenis Kelamin</label>
					<div class="col-md-5">
						<input id="gender" placeholder="Jenis kelamin anda" class="form-control input-md" required="" type="text" value="<?php echo $user->gender == 1? 'Laki-laki' : 'Perempuan'; ?>" readonly>
						<input type="hidden" name="gender" value="<?php echo $user->gender; ?>">
					</div>
				</div>
				<!-- Text input-->
				<div class="form-group">
					<label class="col-md-4 control-label" for="dob">Tanggal Lahir</label>
					<div class="col-md-4">
						<input id="dob" name="dob" placeholder="Tanggal lahir anda" class="form-control input-md datepicker" required="" readonly="" type="text" value="<?php echo date('m/d/Y', strtotime($user->dob)); ?>">
					</div>
				</div>
				<!-- Text input-->
				<div class="form-group">
					<label class="col-md-4 control-label" for="phone">Nomor Kontak</label>
					<div class="col-md-5">
						<input id="phone" name="phone" placeholder="Nomor telepon atau handphone" class="form-control input-md" required="" type="text" value="<?php echo $user->phone; ?>">
						<span class="help-block">ex: 0215123123 atau 081234567</span>
					</div>
				</div>
				<!-- Textarea -->
				<div class="form-group">
					<label class="col-md-4 control-label" for="address">Alamat Lengkap</label>
					<div class="col-md-4">
						<textarea class="form-control" id="address" name="address" required=""><?php echo $user->address; ?></textarea>
					</div>
				</div>
				<!-- Multiple Checkboxes (inline) -->
				<div class="form-group">
					<label class="col-md-4 control-label" for="is_merchant">Apakah anda seorang merchant?</label>
					<div class="col-md-4">
						<label class="checkbox-inline" for="is_merchant">
							<input name="is_merchant" id="merchant" value="1" type="checkbox" <?php echo $user->is_merchant? 'checked' : ''; ?>>
							Ya
						</label>
					</div>
				</div>
				<!-- Text input-->
				<div class="form-group" id="company-name-div" <?php echo $user->is_merchant? '' : 'style="display:none"'; ?>>
					<label class="col-md-4 control-label" for="company_name"></label>
					<div class="col-md-4">
						<input id="company_name" name="company_name" placeholder="Nama perusahaan anda" class="form-control input-md" type="text" value="<?php echo $user->company_name; ?>">
					</div>
				</div>

				<?php if($this->session->has_userdata('error')) {?>
				<div class="alert alert-danger">
				    <?php echo $this->session->flashdata('error'); ?>
				</div>
				<?php } ?>

				<div class="text-center">
					<br><br>
					<button type="submit" class="btn1 btn-blue">Ubah Profil</button>
					<button type="reset" class="btn1 btn-blue-fill">Reset</button>
				</div>

			</fieldset>
		</form>
	</div>

	<?php include('/../partials/footer.php') ?>