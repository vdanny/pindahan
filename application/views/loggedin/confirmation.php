<?php include('/../partials/header.php') ?>

	<div class="container" style="padding-top: 130px; ">

	<ol class="breadcrumb">
		<li><a href="<?php echo site_url('myorder/index'); ?>">Order</a></li>
		<li class="active"><a href="#">Konfirmasi Pembayaran</a></li>
	</ol>

	<div class="row">
		
		<h3>
			Konfirmasi Pembayaran untuk Order #<?php echo $order->id; ?>
		</h3>
		<form class="form-horizontal" id="form-signin" style="margin-top: 30px;" method="POST" action="<?php echo site_url('myorder/doConfirm'); ?>">
			
			<ul class="order-list">
				<li>
					<time datetime="<?php echo date('Y-m-d H:i', strtotime($order->moving_datetime)); ?>">
						<span class="day"><?php echo date('d', strtotime($order->moving_datetime)); ?></span>
						<span class="month"><?php echo date('M', strtotime($order->moving_datetime)); ?></span>
						<span class="year"><?php echo date('Y', strtotime($order->moving_datetime)); ?></span>
						<span class="time"><?php echo date('H:i', strtotime($order->moving_datetime)); ?></span>
					</time>
					<div class="info">
						<h2 class="title"><?php echo $order->fullname . ' - ' . $order->phone; ?></h2>
						<p class="desc"><i class="fa fa-map-marker"></i> Awal: <?php echo $order->pickup_address; ?></p>
						<p class="desc"><i class="fa fa-map-marker"></i> Akhir: <?php echo $order->destination_address; ?></p>

						<p class="desc">Jarak: <?php echo number_format($order->distance/1000, 2); ?> km</p>
						<p class="desc">
							<?php echo $order->package->name.' (Rp ' . number_format($order->price, 2, ',', '.').')'; ?>
						</p>
					</div>
				</li>
			</ul>


			<input type="hidden" name="order_id" value="<?php echo $order->id; ?>">
			<!-- Select-->
			<div class="form-group">
			    <label class="col-md-4 control-label" for="bank_id">Pembayaran kepada</label>
			    <div class="col-md-5">
			        <select class="form-control" name="bank_id">
			        	<?php foreach ($banks as $b): ?>
			        	<option value="<?php echo $b->id ?>">
						<?php echo $b->bank_name . ' - ' . $b->account_name . ' (' . $b->account_no . ')'; ?>
			        	</option>
			        	<?php endforeach ?>
			        </select>
			    </div>
			</div>

			<!-- Text input-->
			<div class="form-group">
			    <label class="col-md-4 control-label" for="bank_name">Nama Bank</label>
			    <div class="col-md-4">
			        <input id="bank_name" name="bank_name" placeholder="Nama bank yang anda gunakan" class="form-control input-md" type="text" maxlength="100" required="">
			        <p class="help-block">(contoh: Bank BCA, Bank Mandiri)</p>
			    </div>
			</div>

			<!-- Text input-->
			<div class="form-group">
			    <label class="col-md-4 control-label" for="account_name">Nama Akun Bank</label>
			    <div class="col-md-4">
			        <input id="account_name" name="account_name" placeholder="Nama nasabah rekening bank anda" class="form-control input-md" type="text" maxlength="100" required="">
			        
			    </div>
			</div>

			<!-- Text input-->
			<div class="form-group">
			    <label class="col-md-4 control-label" for="amount">Jumlah Dibayar</label>
			    <div class="col-md-4">
			        <input id="amount" name="amount" placeholder="Jumlah uang yang anda bayar" class="form-control input-md" type="number" maxlength="15" min="<?php echo $order->price/2; ?>" max="<?php echo $order->price/2; ?>">
			        <p class="help-block">Jumlah yang anda bayarkan adalah 50% dari harga total Rp <?php echo number_format($order->price/2, 2, ',', '.'); ?></p>
			    </div>
			</div>

			<!-- Text input-->
			<div class="form-group">
			    <label class="col-md-4 control-label" for="note">Catatan/Keterangan</label>
			    <div class="col-md-4">
			        <textarea id="note" name="note" placeholder="Catatan atau keterangan untuk kami" class="form-control input-md" maxlength="100"></textarea>
			        
			    </div>
			</div>

			<div class="text-center">
				<br><br>
				<button type="submit" class="btn1 btn-blue">Kirim</button>
				<button type="reset" class="btn1 btn-blue-fill">Reset</button>
			</div>
		</form>

	</div>

	</div>

<?php include('/../partials/footer.php') ?>