<?php include('/../partials/header.php') ?>

	<div class="container" style="padding-top: 130px; min-height: 600px;">

	<ol class="breadcrumb">
		<li><a href="<?php echo site_url('myorder/index'); ?>">Order</a></li>
		<li class="active"><a href="#">Detail</a></li>
	</ol>

	<div class="row">
		<h3>
			Daftar Perabotan
		</h3>
		<?php if (count($appliances) == 0): ?>
		<h4>Anda tidak memiliki perabotan yang terdaftar pada pesanan.</h4>
		<a href="<?php echo site_url('myorder/index'); ?>">Kembali ke halaman sebelumnya.</a>
		<?php else: ?>
		<table class="table table-stripped table-hover table-bordered" style="width: auto;">
			<thead>
				<tr>
					<th class="col-md-3">Nama</th>
					<th class="col-md-4">Ukuran</th>
					<th class="col-md-1">Jumlah</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($appliances as $a): ?>
				<tr>
					<td><?php echo $a->appliance->name; ?></td>
					<td><?php echo $a->size->name; ?> <i>(<?php echo $a->size->dimension ?>)</i></td>
					<td><?php echo $a->qty; ?></td>
				</tr>
				<?php endforeach ?>
			</tbody>
		</table>
		<?php endif ?>

	</div>

	</div>

<?php include('/../partials/footer.php') ?>