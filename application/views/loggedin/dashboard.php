	<?php include('/../partials/header.php') ?>

	<div class="container" style="padding-top: 130px; min-height: 600px;">
		
		<div class="row">
			<h4>Halo, <?php echo $this->session->userdata('user')->fullname; ?>!</h4>
		</div>

		<div class="row">
			<div class="well well-lg">
				<h3>Dashboard</h3>

				<?php if ($last_order): ?>
					There is order
				<?php else: ?>
					<p>Anda belum pernah pindahan. <a href="<?php echo site_url('order'); ?>">Pindah sekarang dengan kami</a></p>
				<?php endif ?>
			</div>
		</div>
	</div>

	<?php include('/../partials/footer.php') ?>