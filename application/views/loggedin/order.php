	<?php include('/../partials/header.php') ?>

	<div class="container" style="padding-top: 130px; padding-bottom: 30px;">
		
		<div class="row">
			<h4>Halo, <?php echo $this->session->userdata('user')->fullname; ?>!</h4>
		</div>

		<div class="row">

			<h3>
				Order Anda
			</h3>
			
			<div class="row" style="padding-bottom: 10px;">
				<div class="[ col-xs-12 col-sm-11 ]">
					<a href="<?php echo site_url('order/index'); ?>" class="btn1 btn-blue pull-right">Pesan Sekarang</a>	
				</div>
			</div>

			<div class="[ col-xs-12 col-sm-11 ]">
				<ul class="order-list">

					<?php foreach ($orders as $o): ?>
					<li>
						<time datetime="<?php echo date('Y-m-d H:i', strtotime($o->moving_datetime)); ?>">
							<span class="day"><?php echo date('d', strtotime($o->moving_datetime)); ?></span>
							<span class="month"><?php echo date('M', strtotime($o->moving_datetime)); ?></span>
							<span class="year"><?php echo date('Y', strtotime($o->moving_datetime)); ?></span>
							<span class="time"><?php echo date('H:i', strtotime($o->moving_datetime)); ?></span>
						</time>
						<div class="info">
							<h2 class="title"><?php echo $o->fullname . ' - ' . $o->phone; ?></h2>
							<!-- <p class="desc">Dari: <?php echo strlen($o->pickup_address) < 60? $o->pickup_address : substr($o->pickup_address, 0, 57).'...'; ?></p> -->
							<!-- <p class="desc">Menuju: <?php echo strlen($o->destination_address) < 60? $o->destination_address : substr($o->destination_address, 0, 57).'...'; ?></p> -->
							<p class="desc"><i class="fa fa-map-marker"></i> Awal: <?php echo $o->pickup_address; ?></p>
							<p class="desc"><i class="fa fa-map-marker"></i> Akhir: <?php echo $o->destination_address; ?></p>

							<p class="desc">Jarak: <?php echo number_format($o->distance/1000, 2); ?> km</p>
							<p class="desc">
								<?php echo $o->package->name.' (Rp ' . number_format($o->price, 2, ',', '.').')'; ?>
							</p>
							<ul>
								<li style="width:30%;"><a href="<?php echo site_url('myorder/detail/'.$o->id); ?>"><i class="glyphicon glyphicon-list-alt"></i> Lihat detail</a></li>
								<li style="width:30%;">
								<?php if ($o->payment_status == 0): ?>
									<a href="<?php echo site_url('myorder/confirmation/'.$o->id) ?>"><i class="fa fa-money"></i> Konfirmasi Bayar</a>
								<?php elseif($o->payment_status == 1): ?>
									<i class="fa fa-money"></i> Pembayaran DP sudah dikonfirmasi
								<?php elseif($o->payment_status == 2): ?>
									<i class="fa fa-money"></i> Pembayaran DP sudah lunas
								<?php endif ?>
								</li>
							</ul>
						</div>
					</li>
					<?php endforeach ?>
				</ul>	
				
			</div>
			

		</div>
	</div>

	<?php include('/../partials/footer.php') ?>