
	<?php include('partials/header.php'); ?>

	<div class="container">
		
		<form class="form-horizontal" id="form-signup" method="POST" action="<?php echo site_url('signup/doSignup'); ?>">
			<fieldset>
				<!-- Form Name -->
				<h2 align="center">Daftar Member</h2><br><br><br>
				

				<!-- Text input-->
				<div class="form-group">
					<label class="col-md-4 control-label" for="email">Email</label>
					<div class="col-md-5">
						<input id="email" name="email" placeholder="Alamat email aktif anda" class="form-control input-md" type="email" required="">
						<span class="help-block">ex: pindah@pindahan.com</span>
					</div>
				</div>
				<!-- Password input-->
				<div class="form-group">
					<label class="col-md-4 control-label" for="password">Password</label>
					<div class="col-md-4">
						<input id="password" name="password" placeholder="Password akun anda" class="form-control input-md" required="" type="password">
					</div>
				</div>
				<!-- Password input-->
				<div class="form-group">
					<label class="col-md-4 control-label" for="confirm-password">Konfirmasi Password</label>
					<div class="col-md-4">
						<input id="confirm-password" name="confirm-password" placeholder="Konfirmasi password sebelumnya" class="form-control input-md" required="" type="password">
					</div>
				</div>
				<!-- Text input-->
				<div class="form-group">
					<label class="col-md-4 control-label" for="fullname">Nama Lengkap</label>
					<div class="col-md-5">
						<input id="fullname" name="fullname" placeholder="Nama lengkap anda" class="form-control input-md" required="" type="text">
					</div>
				</div>
				<!-- Select Basic -->
				<div class="form-group">
					<label class="col-md-4 control-label" for="gender">Jenis Kelamin</label>
					<div class="col-md-4">
						<select id="gender" name="gender" class="form-control" required="">
							<option value="1">Laki-laki</option>
							<option value="2">Perempuan</option>
						</select>
					</div>
				</div>
				<!-- Text input-->
				<div class="form-group">
					<label class="col-md-4 control-label" for="dob">Tanggal Lahir</label>
					<div class="col-md-4">
						<input id="dob" name="dob" placeholder="Tanggal lahir anda" class="form-control input-md datepicker" required="" readonly="" type="text">
					</div>
				</div>
				<!-- Text input-->
				<div class="form-group">
					<label class="col-md-4 control-label" for="phone">Nomor Kontak</label>
					<div class="col-md-5">
						<input id="phone" name="phone" placeholder="Nomor telepon atau handphone" class="form-control input-md" required="" type="text">
						<span class="help-block">ex: 0215123123 atau 081234567</span>
					</div>
				</div>
				<!-- Textarea -->
				<div class="form-group">
					<label class="col-md-4 control-label" for="address">Alamat Lengkap</label>
					<div class="col-md-4">
						<textarea class="form-control" id="address" name="address" required=""></textarea>
					</div>
				</div>
				<!-- Multiple Checkboxes (inline) -->
				<div class="form-group">
					<label class="col-md-4 control-label" for="is_merchant">Apakah anda seorang merchant?</label>
					<div class="col-md-4">
						<label class="checkbox-inline" for="is_merchant">
							<input name="is_merchant" id="merchant" value="1" type="checkbox" checked="">
							Ya
						</label>
					</div>
				</div>
				<!-- Text input-->
				<div class="form-group" id="company-name-div">
					<label class="col-md-4 control-label" for="company_name"></label>
					<div class="col-md-4">
						<input id="company_name" name="company_name" placeholder="Nama perusahaan anda" class="form-control input-md" type="text">
					</div>
				</div>


				<div class="text-center">
					<small class="help-block">Dengan mendaftar sebagai member Pindahan, anda telah menyetujui <a href="#">Syarat & Ketentuan</a> yang berlaku</small>
					<br><br>
					<button type="submit" class="btn1 btn-blue">Daftar</button>
					<button type="reset" class="btn1 btn-blue-fill">Reset</button>
				</div>

			</fieldset>
		</form>
	</div>

	<?php include('partials/footer.php'); ?>