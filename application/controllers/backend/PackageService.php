<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PackageService extends CI_Controller 
{

	private $master = 'backend/master';
	private $rootview = 'backend/packageservice';

	function __construct()
	{
		parent::__construct();
		if(!$this->session->has_userdata('admin')) redirect('backend/login');
		$this->load->model('packages');
	}

	public function index()
	{
		$data['packages'] = $this->packages->get();
		$data['content'] = $this->rootview . '/index';
		$this->load->view($this->master, $data);
	}

	public function add()
	{
		$data['content'] = $this->rootview . '/add';
		$this->load->view($this->master, $data);
	}

	public function edit($id)
	{
		$data['content'] = $this->rootview . '/edit';
		$data['package'] = $this->packages->get($id);
		$this->load->view($this->master, $data);
	}

	public function delete($id)
	{
		if($this->packages->delete($id))
		{
			$this->session->set_flashdata('msg', 'Success delete package');
		}
		else 
		{
			$this->session->set_flashdata('msg', 'Failed to delete package');
		}
		return redirect('backend/packageservice/index');
	}

	public function doCreate()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Package Name', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('transportation', 'Transportation', 'trim|required|numeric');
		$this->form_validation->set_rules('distance', 'Distance', 'trim|required|numeric');
		$this->form_validation->set_rules('move_emp', 'Mover Employee', 'trim|required|numeric');
		$this->form_validation->set_rules('pack_emp', 'Packer Employee', 'trim|required|numeric');
		$this->form_validation->set_rules('unpack_emp', 'Unpacker Employee', 'trim|required|numeric');
		$this->form_validation->set_rules('description', 'Description', 'trim|max_length[500]');
		$this->form_validation->set_rules('price', 'Price', 'trim|required|numeric');

		if($this->form_validation->run() == FALSE) 
		{
			$this->session->set_flashdata('error', validation_errors());
			redirect('backend/packageservice/add');
		}
		else
		{
			$data = $this->input->post();
			$data['modified_by'] = $this->session->userdata('admin')->username;

			// create user if validation success
			if($this->packages->create($data))
			{
				$this->session->set_flashdata('msg', 'Success add new package');
			}
			else 
			{
				$this->session->set_flashdata('msg', 'Failed to add new package');	
			}
			redirect('backend/packageservice/index');
		}
	}

	public function doEdit()
	{
		$this->load->library('form_validation');
		$id = $this->input->post('id');

		$this->form_validation->set_rules('name', 'Package Name', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('transportation', 'Transportation', 'trim|required|numeric');
		$this->form_validation->set_rules('distance', 'Distance', 'trim|required|numeric');
		$this->form_validation->set_rules('move_emp', 'Mover Employee', 'trim|required|numeric');
		$this->form_validation->set_rules('pack_emp', 'Packer Employee', 'trim|required|numeric');
		$this->form_validation->set_rules('unpack_emp', 'Unpacker Employee', 'trim|required|numeric');
		$this->form_validation->set_rules('description', 'Description', 'trim|max_length[500]');
		$this->form_validation->set_rules('price', 'Price', 'trim|required|numeric');

		if($this->form_validation->run() == FALSE) 
		{
			$this->session->set_flashdata('error', validation_errors());
			redirect('backend/packageservice/edit/' . $id);
		}
		else
		{
			// update user if validation success
			$data = $this->input->post();
			$data['modified_by'] = $this->session->userdata('admin')->username;

			if($this->packages->update($data))
			{
				$this->session->set_flashdata('msg', 'Success edit package');
			}
			else 
			{
				$this->session->set_flashdata('msg', 'Failed to edit package');	
			}
			redirect('backend/packageservice/index');
		}
	}
}