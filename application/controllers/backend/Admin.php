<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller 
{
	private $master = 'backend/master';
	private $rootview = 'backend/admin';

	function __construct()
	{
		parent::__construct();
		if(!$this->session->has_userdata('admin')) redirect('backend/login');
		$this->load->model('administrators');
	}

	public function index() 
	{
		$data['admins'] = $this->administrators->get();
		$data['content'] = $this->rootview . '/index';
		$this->load->view($this->master, $data);
	}

	public function add()
	{
		$data['content'] = $this->rootview . '/add';
		$this->load->view($this->master, $data);
	}

	public function	edit($id)
	{
		$data['content'] = $this->rootview . '/edit';
		$data['admin'] = $this->administrators->get($id);
		$this->load->view($this->master, $data);
	}

	public function delete($id)
	{
		if($this->administrators->delete($id))
		{
			$this->session->set_flashdata('msg', 'Success delete administrator');
		}
		else 
		{
			$this->session->set_flashdata('msg', 'Failed to delete administrator');
		}
		return redirect('backend/admin/index');
	}

	public function doCreate()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[5]|max_length[50]|is_unique[administrators.username]');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[5]|max_length[50]');
		$this->form_validation->set_rules('confirm-password', 'Password Confirmation', 'trim|required|matches[password]');

		if($this->form_validation->run() == FALSE) 
		{
			// $data['content'] = $this->rootview . '/add';
			// $this->load->view($this->master, $data);
			$this->session->set_flashdata('error', validation_errors());
			redirect('backend/admin/add');
		}
		else
		{
			// create user if validation success
			$d = array(
				'username' => $this->input->post('username'),
				'password' => $this->input->post('password'),
				'is_super' => $this->input->post('is_super') !== NULL? $this->input->post('is_super') : 0
				);
			if($this->administrators->create($d))
			{
				$this->session->set_flashdata('msg', 'Success add new administrator');
			}
			else 
			{
				$this->session->set_flashdata('msg', 'Failed to add new administrator');	
			}
			redirect('backend/admin/index');
		}
	}

	public function doEdit()
	{
		$this->load->library('form_validation');
		$id = $this->input->post('id');

		$this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[5]|max_length[50]|callback_edit_unique[administrators.username.'. $id .']');
		$this->form_validation->set_rules('password', 'Password', 'trim|min_length[5]|max_length[50]');
		$this->form_validation->set_rules('confirm-password', 'Password Confirmation', 'trim|matches[password]');

		if($this->form_validation->run() == FALSE) 
		{
			$this->session->set_flashdata('error', validation_errors());
			redirect('backend/admin/edit/' . $id);
		}
		else
		{
			// update user if validation success
			$d = array(
				'id'	   => $id,
				'username' => $this->input->post('username'),
				'is_super' => $this->input->post('is_super') !== NULL? $this->input->post('is_super') : 0
				);
			// set password if exists
			if($this->input->post('password') !== '') $d['password'] = $this->input->post('password');

			if($this->administrators->update($d))
			{
				$this->session->set_flashdata('msg', 'Success edit administrator');
			}
			else 
			{
				$this->session->set_flashdata('msg', 'Failed to edit administrator');	
			}
			redirect('backend/admin/index');
		}
	}

	public function edit_unique($value, $params)
	{
	    $this->form_validation->set_message('edit_unique',
	        'The %s is already being used by another account.');

	    list($table, $field, $id) = explode(".", $params, 3);

	    $query = $this->db->select($field)->from($table)
	        ->where($field, $value)->where('id !=', $id)->limit(1)->get();

	    if ($query->row()) {
	        return false;
	    } else {
	        return true;
	    }
	}

}