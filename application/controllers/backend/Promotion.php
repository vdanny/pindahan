<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Promotion extends CI_Controller 
{

	private $master = 'backend/master';
	private $rootview = 'backend/promotion';
	private $upload_path = "./uploads/promotions/";

	function __construct()
	{
		parent::__construct();
		if(!$this->session->has_userdata('admin')) redirect('backend/login');
		$this->load->model('promotions');
	}

	public function index()
	{
		$data['promotions'] = $this->promotions->get();
		$data['content'] = $this->rootview . '/index';
		$this->load->view($this->master, $data);
	}

	public function add()
	{
		$data['content'] = $this->rootview . '/add';
		$this->load->view($this->master, $data);
	}

	public function edit($id)
	{
		$data['content'] = $this->rootview . '/edit';
		$data['promotion'] = $this->promotions->get($id);
		$this->load->view($this->master, $data);
	}

	public function delete($id)
	{
		$promo = $this->promotions->get($id);
		$filepath = $this->upload_path . $promo->image_path;
		if($this->promotions->delete($id))
		{
			unlink($filepath);
			$this->session->set_flashdata('msg', 'Success delete promotion');
		}
		else 
		{
			$this->session->set_flashdata('msg', 'Failed to delete promotion');
		}
		return redirect('backend/promotion/index');
	}

	public function doCreate()
	{
		$config = array(
			'upload_path' => $this->upload_path,
			'allowed_types' => "gif|jpg|png|jpeg",
			'overwrite' => TRUE,
			'max_size' => "" . (2 * 1024 * 1024)
		);

		// rename the filename
		$newFileName = $_FILES['image']['name'];
		$fileExt = array_pop(explode(".", $newFileName));
		$filename = md5(time()) . "." . $fileExt;

		//set filename in config for upload
		$config['file_name'] = $filename;

		$this->load->library('upload', $config);

		if($this->upload->do_upload('image'))
		{
			$data['image_path'] = $this->upload->data('file_name');
			$data['modified_by'] = $this->session->userdata('admin')->username;

			if($this->promotions->create($data))
			{
				$this->session->set_flashdata('msg', 'Success add new promotion');
			}
			else 
			{
				$this->session->set_flashdata('msg', 'Failed to add new promotion');
				$filepath = $this->upload_path . $data['image_path'];
				unlink($filepath);
			}
			redirect('backend/promotion/index');
		}
		else
		{
			$this->session->set_flashdata('error', $this->upload->display_errors());
			redirect('backend/promotion/add');
		}
	}

	public function doEdit()
	{
		$config = array(
			'upload_path' => $this->upload_path,
			'allowed_types' => "gif|jpg|png|jpeg",
			'overwrite' => TRUE,
			'max_size' => "" . (2 * 1024 * 1024)
		);

		// rename the filename
		$newFileName = $_FILES['image']['name'];
		$fileExt = array_pop(explode(".", $newFileName));
		$filename = md5(time()) . "." . $fileExt;

		//set filename in config for upload
		$config['file_name'] = $filename;

		// load File Uploading Library
		$this->load->library('upload', $config);

		// get old image_path
		$id = $this->input->post('id');
		$promotion = $this->promotions->get($id);
		$old_filepath = $this->upload_path . $promotion->image_path;

		if($this->upload->do_upload('image'))
		{
			$data['id'] = $id;
			$data['image_path'] = $this->upload->data('file_name');
			$data['modified_by'] = $this->session->userdata('admin')->username;

			if($this->promotions->update($data))
			{
				$this->session->set_flashdata('msg', 'Success edit promotion');
				unlink($old_filepath);
			}
			else 
			{
				$this->session->set_flashdata('msg', 'Failed to edit promotion');
				$filepath = $this->upload_path . $data['image_path'];
				unlink($filepath);
			}
			redirect('backend/promotion/index');
		}
		else
		{
			$this->session->set_flashdata('error', $this->upload->display_errors());
			redirect('backend/promotion/add');
		}
	}
}