<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends CI_Controller 
{
	private $master = 'backend/master';
	private $rootview = 'backend/member';

	function __construct()
	{
		parent::__construct();
		if(!$this->session->has_userdata('admin')) redirect('backend/login');
		$this->load->model('users');
	}

	public function index() 
	{
		$data['members'] = $this->users->get();
		$data['content'] = $this->rootview . '/index';
		$this->load->view($this->master, $data);
	}

	public function add()
	{
		$data['content'] = $this->rootview . '/add';
		$this->load->view($this->master, $data);
	}

	public function	edit($id)
	{
		$data['content'] = $this->rootview . '/edit';
		$data['member'] = $this->users->get($id);
		$this->load->view($this->master, $data);
	}

	public function delete($id)
	{
		if($this->users->delete($id))
		{
			$this->session->set_flashdata('msg', 'Success delete member');
		}
		else 
		{
			$this->session->set_flashdata('msg', 'Failed to delete member');
		}
		return redirect('backend/member/index');
	}

	public function doCreate()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('email', 'Email Address', 'trim|required|max_length[100]|valid_email|is_unique[users.email]');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[5]|max_length[50]');
		$this->form_validation->set_rules('confirm-password', 'Password Confirmation', 'trim|required|matches[password]');
		$this->form_validation->set_rules('fullname', 'Full Name', 'trim|required|min_length[3]|max_length[100]');
		$this->form_validation->set_rules('gender', 'Gender', 'required');
		$this->form_validation->set_rules('dob', 'Birthdate', 'trim|required|min_length[5]|max_length[50]');
		$this->form_validation->set_rules('phone', 'Phone Number', 'trim|required|min_length[5]|max_length[20]');
		$this->form_validation->set_rules('address', 'Address', 'trim|required|max_length[500]');

		if($this->form_validation->run() == FALSE) 
		{
			$this->session->set_flashdata('error', validation_errors());
			redirect('backend/member/add');
		}
		else
		{
			// create user if validation success
			$d = $this->input->post();
			$d['dob'] = date('Y-m-d', strtotime($d['dob']));
			unset($d['confirm-password']);
			if($this->users->create($d))
			{
				$this->session->set_flashdata('msg', 'Success add new member');
			}
			else 
			{
				$this->session->set_flashdata('msg', 'Failed to add new member');	
			}
			redirect('backend/member/index');
		}
	}

	public function doEdit()
	{
		$this->load->library('form_validation');
		$id = $this->input->post('id');

		$this->form_validation->set_rules('email', 'Email Address', 'trim|required|max_length[100]|valid_email|callback_edit_unique[users.email.' . $id . ']');
		$this->form_validation->set_rules('password', 'Password', 'trim|min_length[5]|max_length[50]');
		$this->form_validation->set_rules('confirm-password', 'Password Confirmation', 'trim|matches[password]');
		$this->form_validation->set_rules('fullname', 'Full Name', 'trim|required|min_length[3]|max_length[100]');
		$this->form_validation->set_rules('gender', 'Gender', 'required');
		$this->form_validation->set_rules('dob', 'Birthdate', 'trim|required|min_length[5]|max_length[50]');
		$this->form_validation->set_rules('phone', 'Phone Number', 'trim|required|min_length[5]|max_length[20]');
		$this->form_validation->set_rules('address', 'Address', 'trim|required|max_length[500]');

		if($this->form_validation->run() == FALSE) 
		{
			$this->session->set_flashdata('error', validation_errors());
			redirect('backend/member/edit/' . $id);
		}
		else
		{
			// update user if validation success
			$d = $this->input->post();
			$d['dob'] = date('Y-m-d', strtotime($d['dob']));
			// set password if exists
			if($this->input->post('password') === '') unset($d['password']);

			if($this->users->update($d))
			{
				$this->session->set_flashdata('msg', 'Success edit member');
			}
			else 
			{
				$this->session->set_flashdata('msg', 'Failed to edit member');	
			}
			redirect('backend/member/index');
		}
	}

	public function edit_unique($value, $params)
	{
	    $this->form_validation->set_message('edit_unique',
	        'The %s is already being used by another account.');

	    list($table, $field, $id) = explode(".", $params, 3);

	    $query = $this->db->select($field)->from($table)
	        ->where($field, $value)->where('id !=', $id)->limit(1)->get();

	    if ($query->row()) {
	        return false;
	    } else {
	        return true;
	    }
	}

}