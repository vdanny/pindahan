<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Appliance extends CI_Controller 
{

	private $master = 'backend/master';
	private $rootview = 'backend/appliance';

	function __construct()
	{
		parent::__construct();
		if(!$this->session->has_userdata('admin')) redirect('backend/login');
		$this->load->model('appliances');
		$this->load->model('sizes');
	}

	public function index()
	{
		$data['appliances'] = $this->appliances->get();
		$data['content'] = $this->rootview . '/index';
		$this->load->view($this->master, $data);
	}

	public function add()
	{
		$data['content'] = $this->rootview . '/add';
		$this->load->view($this->master, $data);
	}

	public function edit($id)
	{
		$data['content'] = $this->rootview . '/edit';
		$data['appliance'] = $this->appliances->get($id);
		$this->load->view($this->master, $data);
	}

	public function delete($id)
	{
		if($this->appliances->delete($id))
		{
			$this->session->set_flashdata('msg', 'Success delete appliance');
		}
		else 
		{
			$this->session->set_flashdata('msg', 'Failed to delete appliance');
		}
		return redirect('backend/appliance/index');
	}
	
	public function size($id)
	{
		$data['appliance'] = $this->appliances->get($id);
		$data['sizes'] = $this->sizes->get($id);
		$data['content'] = $this->rootview . '/size';
		$this->load->view($this->master, $data);
	}

	public function add_size($id)
	{
		$data['appliance'] = $this->appliances->get($id);
		$data['content'] = $this->rootview . '/add_size';
		$this->load->view($this->master, $data);
	}

	public function edit_size($appliance_id, $size_id)
	{
		$data['appliance'] = $this->appliances->get($appliance_id);
		$data['size'] = $this->sizes->get_by_id($size_id);
		$data['content'] = $this->rootview . '/edit_size';
		$this->load->view($this->master, $data);
	}

	public function delete_size($appliance_id, $size_id)
	{
		if($this->sizes->delete($size_id))
		{
			$this->session->set_flashdata('msg', 'Success delete size');
		}
		else 
		{
			$this->session->set_flashdata('msg', 'Failed to delete size');
		}
		return redirect('backend/appliance/size/' . $appliance_id);
	}

	public function doCreate()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Name', 'trim|required|max_length[100]');

		if($this->form_validation->run() == FALSE) 
		{
			$this->session->set_flashdata('error', validation_errors());
			redirect('backend/appliance/add');
		}
		else
		{
			$data = $this->input->post();

			// create user if validation success
			if($this->appliances->create($data))
			{
				$this->session->set_flashdata('msg', 'Success add new appliance');
			}
			else 
			{
				$this->session->set_flashdata('msg', 'Failed to add new appliance');	
			}
			redirect('backend/appliance/index');
		}
	}

	public function doEdit()
	{
		$this->load->library('form_validation');
		$id = $this->input->post('id');

		$this->form_validation->set_rules('name', 'Name', 'trim|required|max_length[100]');

		if($this->form_validation->run() == FALSE) 
		{
			$this->session->set_flashdata('error', validation_errors());
			redirect('backend/appliance/edit/' . $id);
		}
		else
		{
			// update user if validation success
			$data = $this->input->post();

			if($this->appliances->update($data))
			{
				$this->session->set_flashdata('msg', 'Success edit appliance');
			}
			else 
			{
				$this->session->set_flashdata('msg', 'Failed to edit appliance');	
			}
			redirect('backend/appliance/index');
		}
	}

	public function doCreateSize()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Name', 'trim|required|max_length[100]');
		$this->form_validation->set_rules('dimension', 'Dimension', 'trim|required|max_length[100]');

		$appliance_id = $this->input->post('appliance_id');

		if($this->form_validation->run() == FALSE) 
		{
			$this->session->set_flashdata('error', validation_errors());
			redirect('backend/appliance/add_size/' . $appliance_id);
		}
		else
		{
			$data = $this->input->post();

			// create user if validation success
			if($this->sizes->create($data))
			{
				$this->session->set_flashdata('msg', 'Success add new size');
			}
			else 
			{
				$this->session->set_flashdata('msg', 'Failed to add new size');	
			}
			redirect('backend/appliance/size/' . $appliance_id);
		}
	}

	public function doEditSize()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Name', 'trim|required|max_length[100]');
		$this->form_validation->set_rules('dimension', 'Dimension', 'trim|required|max_length[100]');

		$appliance_id = $this->input->post('appliance_id');

		if($this->form_validation->run() == FALSE) 
		{
			$this->session->set_flashdata('error', validation_errors());
			redirect('backend/appliance/edit_size/' . $appliance_id);
		}
		else
		{
			$data = $this->input->post();

			// create user if validation success
			if($this->sizes->update($data))
			{
				$this->session->set_flashdata('msg', 'Success edit size');
			}
			else 
			{
				$this->session->set_flashdata('msg', 'Failed to edit size');	
			}
			redirect('backend/appliance/size/' . $appliance_id);
		}
	}
}