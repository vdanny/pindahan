<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends CI_Controller 
{

	private $master = 'backend/master';
	private $rootview = 'backend/order';

	function __construct()
	{
		parent::__construct();
		if(!$this->session->has_userdata('admin')) redirect('backend/login');
		$this->load->model('orders');
		$this->load->model('packages');
		$this->load->model('confirmations');
	}

	public function index()
	{
		$data['orders'] = $this->orders->get();
		$data['content'] = $this->rootview . '/index';
		$this->load->view($this->master, $data);
	}

	public function add()
	{
		$data['content'] = $this->rootview . '/add';
		$this->load->view($this->master, $data);
	}

	public function edit($id)
	{
		$data['content'] = $this->rootview . '/edit';
		$data['order'] = $this->orders->get($id);
		$data['packages'] = $this->packages->get();
		$this->load->view($this->master, $data);
	}

	public function delete($id)
	{
		if($this->orders->delete($id))
		{
			$this->session->set_flashdata('msg', 'Success delete order');
		}
		else 
		{
			$this->session->set_flashdata('msg', 'Failed to delete order');
		}
		return redirect('backend/order/index');
	}

	public function detail($id)
	{
		$data['content'] = $this->rootview . '/detail';
		$data['order'] = $this->orders->get($id);
		$data['details'] = $this->orders->get_details($id);
		$this->load->view($this->master, $data);
	}

	public function doEdit()
	{
		$this->load->library('form_validation');
		$id = $this->input->post('id');

		$this->form_validation->set_rules('fullname', 'Full Name', 'trim|required|max_length[250]');
		$this->form_validation->set_rules('phone', 'Phone', 'trim|required|numeric|max_length[20]');
		$this->form_validation->set_rules('pickup_address', 'Pickup Address', 'trim|required|max_length[500]');
		$this->form_validation->set_rules('destination_address', 'Destination Address', 'trim|required|max_length[500]');
		$this->form_validation->set_rules('package_id', 'Package', 'trim|required|numeric');
		$this->form_validation->set_rules('price', 'Price', 'trim|required|numeric');
		$this->form_validation->set_rules('distance', 'Distance', 'trim|required|numeric');

		if($this->form_validation->run() == FALSE) 
		{
			$this->session->set_flashdata('error', validation_errors());
			redirect('backend/order/edit/' . $id);
		}
		else
		{
			$msg = '';
			// update user if validation success
			$data = $this->input->post();
			$data['modified_by'] = $this->session->userdata('admin')->username;

			// recount price
			if($data['package_id'] != 0){
				$package = $this->packages->get($data['package_id']);
				$package_price = $package->price;
				$free_distance = $package->distance * 1000; // in kilometers
				$distance = $data['distance'];
				$price_per_km = $this->orders->get_distance_price()->price_per_km;

				$paid_distance = $distance > $free_distance? ($distance - $free_distance) : 0;
				$paid_distance = ceil($paid_distance/1000);
				$distance_price = $paid_distance * $price_per_km;

				$data['price'] = $package_price + $distance_price;
			}
			else {
				$data['price'] = 0;
				// add message to admin
				$msg = '. Don\'t forget to edit order details to recalculate the price!';
			}


			if($this->orders->update($data))
			{
				$this->session->set_flashdata('msg', 'Success edit order' . $msg);
			}
			else 
			{
				$this->session->set_flashdata('msg', 'Failed to edit order');	
			}
			redirect('backend/order/index');
		}
	}

	public function appliances($order_id)
	{
		$data['content'] = $this->rootview . '/appliance';
		$data['order'] = $this->orders->get($order_id);
		$data['appliances'] = $this->orders->get_appliance_details($order_id);
		$this->load->view($this->master, $data);	
	}

	public function confirmation($order_id)
	{
		$data['content'] = $this->rootview . '/confirmation';
		$data['order'] = $this->orders->get($order_id);
		$data['confirm'] = $this->confirmations->get_by_order_id($order_id);
		$this->load->view($this->master, $data);
	}

	public function change_status($id, $status)
	{
		if($this->orders->update_status($id, $status))
		{
			$this->session->set_flashdata('msg', 'Success edit payment status for order #'.$id);
		}
		else
		{
			$this->session->set_flashdata('msg', 'Failed to edit payment status for order #'.$id);
		}
		redirect('backend/order/index');
	}
}