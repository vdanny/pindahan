<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CustomService extends CI_Controller 
{

	private $master = 'backend/master';
	private $rootview = 'backend/customservice';

	function __construct()
	{
		parent::__construct();
		if(!$this->session->has_userdata('admin')) redirect('backend/login');
		$this->load->model('customprices');
	}

	public function index()
	{
		$data['prices'] = $this->customprices->get();
		$data['content'] = $this->rootview . '/index';
		$this->load->view($this->master, $data);
	}

	public function add()
	{
		$data['content'] = $this->rootview . '/add';
		$this->load->view($this->master, $data);
	}

	public function edit($id)
	{
		$data['content'] = $this->rootview . '/edit';
		$data['price'] = $this->customprices->get($id);
		$this->load->view($this->master, $data);
	}

	public function delete($id)
	{
		if($this->customprices->delete($id))
		{
			$this->session->set_flashdata('msg', 'Success delete custom price');
		}
		else 
		{
			$this->session->set_flashdata('msg', 'Failed to delete custom price');
		}
		return redirect('backend/customservice/index');
	}

	public function doCreate()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Name', 'trim|required|max_length[100]');
		$this->form_validation->set_rules('price', 'Price', 'trim|required|numeric');

		if($this->form_validation->run() == FALSE) 
		{
			$this->session->set_flashdata('error', validation_errors());
			redirect('backend/customservice/add');
		}
		else
		{
			$data = $this->input->post();
			$data['modified_by'] = $this->session->userdata('admin')->username;

			// create user if validation success
			if($this->customprices->create($data))
			{
				$this->session->set_flashdata('msg', 'Success add new custom price');
			}
			else 
			{
				$this->session->set_flashdata('msg', 'Failed to add new custom price');	
			}
			redirect('backend/customservice/index');
		}
	}

	public function doEdit()
	{
		$this->load->library('form_validation');
		$id = $this->input->post('id');

		$this->form_validation->set_rules('name', 'Package Name', 'trim|required|max_length[100]');
		$this->form_validation->set_rules('price', 'Price', 'trim|required|numeric');

		if($this->form_validation->run() == FALSE) 
		{
			$this->session->set_flashdata('error', validation_errors());
			redirect('backend/customservice/edit/' . $id);
		}
		else
		{
			// update user if validation success
			$data = $this->input->post();
			$data['modified_by'] = $this->session->userdata('admin')->username;

			if($this->customprices->update($data))
			{
				$this->session->set_flashdata('msg', 'Success edit custom price');
			}
			else 
			{
				$this->session->set_flashdata('msg', 'Failed to edit custom price');	
			}
			redirect('backend/customservice/index');
		}
	}
}