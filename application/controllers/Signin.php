<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Signin extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('users');
	}

	public function index()
	{
		$this->load->view('signin');
	}

	public function doLogin()
	{
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		$user = $this->users->login($email, $password);
		if($user)
		{
			$this->session->set_userdata('user', $user);
			// redirect('dashboard/index');
			redirect('myorder/index');
		}
		else
		{
			$this->session->set_flashdata('msg', 'Your authentication is invalid.');	
			redirect('signin');
		}
	}

	public function logout() 
	{
		$this->session->sess_destroy();
		redirect('signin');
	}

}
