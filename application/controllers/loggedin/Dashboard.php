<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller 
{
	private $rootview = 'loggedin/';

	function __construct()
	{
		parent::__construct();
		$this->load->model('orders');
		if($this->session->has_userdata('user') == FALSE) {
			$this->session->set_flashdata('msg', 'Anda tidak memiliki akses ke halaman tersebut. Harap masuk dengan akun member yang terdaftar.');
			redirect('signin');
		}
	}

	public function index()
	{
		$id = $this->session->userdata('user')->id;
		$data['last_order'] = $this->orders->get_last_order_by_user($id);
		$this->load->view($this->rootview . 'dashboard', $data);
	}

}
