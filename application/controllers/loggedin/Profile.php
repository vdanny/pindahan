<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller 
{
	private $rootview = 'loggedin/';

	function __construct()
	{
		parent::__construct();
		$this->load->model('users');
		if($this->session->has_userdata('user') == FALSE) {
			$this->session->set_flashdata('msg', 'Anda tidak memiliki akses ke halaman tersebut. Harap masuk dengan akun member yang terdaftar.');
			redirect('signin');
		}
	}

	public function index()
	{
		$id = $this->session->userdata('user')->id;
		$data['user'] = $this->users->get($id);
		$this->load->view($this->rootview . 'profile', $data);
	}

	public function doEdit()
	{
		$this->load->library('form_validation');
		$id = $this->input->post('id');
		$this->form_validation->set_rules('email', 'Email Address', 'trim|required|max_length[100]|valid_email|callback_edit_unique[users.email.' . $id . ']');
		$this->form_validation->set_rules('password', 'Password', 'trim|min_length[5]|max_length[100]');
		$this->form_validation->set_rules('confirm-password', 'Password Confirmation', 'trim|min_length[5]|max_length[100]|matches[password]');
		$this->form_validation->set_rules('fullname', 'Full Name', 'trim|required|min_length[3]|max_length[100]');
		$this->form_validation->set_rules('gender', 'Gender', 'required');
		$this->form_validation->set_rules('dob', 'Birthdate', 'trim|required|min_length[5]|max_length[50]');
		$this->form_validation->set_rules('phone', 'Phone Number', 'trim|required|min_length[5]|max_length[20]');
		$this->form_validation->set_rules('address', 'Address', 'trim|required|max_length[500]');

		if($this->form_validation->run() == FALSE) 
		{
			$this->session->set_flashdata('error', validation_errors());
		redirect('profile/index');
		}
		else
		{
			// create user if validation success
			$d = $this->input->post();
			$d['dob'] = date('Y-m-d', strtotime($d['dob']));
			unset($d['confirm-password']);

			if($this->users->update($d))
			{
				$this->session->set_flashdata('msg', 'Profile anda berhasil diperbarui');
			}
			else 
			{
				$this->session->set_flashdata('msg', 'Oops! An error has occured, please contact our customer service :)');	
			}
			redirect('profile/index');
		}
	}

	public function edit_unique($value, $params)
	{
	    $this->form_validation->set_message('edit_unique',
	        'The %s is already being used by another account.');

	    list($table, $field, $id) = explode(".", $params, 3);

	    $query = $this->db->select($field)->from($table)
	        ->where($field, $value)->where('id !=', $id)->limit(1)->get();

	    if ($query->row()) {
	        return false;
	    } else {
	        return true;
	    }
	}

}
