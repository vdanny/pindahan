<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MyOrder extends CI_Controller 
{
	private $rootview = 'loggedin/';

	function __construct()
	{
		parent::__construct();
		$this->load->model('orders');
		$this->load->model('banks');
		$this->load->model('confirmations');
		if($this->session->has_userdata('user') == FALSE) {
			$this->session->set_flashdata('msg', 'Anda tidak memiliki akses ke halaman tersebut. Harap masuk dengan akun member yang terdaftar.');
			redirect('signin');
		}
	}

	public function index()
	{
		$id = $this->session->userdata('user')->id;
		$data['orders'] = $this->orders->get_orders_by_user($id);
		$this->load->view($this->rootview . 'order', $data);
	}

	public function detail($order_id)
	{
		$data['appliances'] = $this->orders->get_appliance_details($order_id);
		$this->load->view($this->rootview . 'orderdetail', $data);
	}

	public function confirmation($order_id)
	{
		$data['order'] = $this->orders->get($order_id);
		$data['banks'] = $this->banks->get();
		$this->load->view($this->rootview . 'confirmation', $data);
	}

	public function doConfirm()
	{
		$data = $this->input->post();
		if($this->confirmations->create($data))
		{
			$this->session->set_flashdata('msg', 'Pembayaran DP anda telah berhasil dikonfirmasi');
		}
		else
		{
			$this->session->set_flashdata('msg', 'Pembayaran DP anda gagal dikonfirmasi');
		}
		redirect('myorder/index');
	}

}
