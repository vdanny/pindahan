<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Backend1 extends CI_Controller 
{
	private $master = 'backend/master';

	function __construct()
	{
		parent::__construct();
		$this->load->model('administrator');
	}

	public function index()
	{
		if($this->session->has_userdata('admin')) {
			$data['content'] = 'backend/index';
			$this->load->view($this->master, $data);
		}
		else {
			redirect('backend/login');
		}
	}

	public function login()
	{
		if($this->session->has_userdata('admin')) {
			redirect('backend');
		}
		else {
			$this->load->view('backend/login');
		}
	}

	public function doLogin() 
	{
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$user = $this->administrator->login($username, $password);

		if($user) {
			$this->session->set_userdata('admin', $user);
			redirect('backend');
		}
		else {
			$this->session->set_flashdata('msg', 'Your authentication is invalid!');
			redirect('backend/login');
		}
	}

	public function logout() 
	{
		$this->session->sess_destroy();
		redirect('backend/login');
	}

	public function test()
	{
		$data['content'] = 'backend/admin/index';
		$this->load->view($this->master, $data);
	}

}
