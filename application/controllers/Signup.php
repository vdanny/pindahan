<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Signup extends CI_Controller 
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('users');
	}

	public function index()
	{
		$this->load->view('signup');
	}

	public function success()
	{
		$this->load->view('signup_success');
	}

	public function doSignup()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('email', 'Email Address', 'trim|required|max_length[100]|valid_email|is_unique[users.email]');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[5]|max_length[100]');
		$this->form_validation->set_rules('confirm-password', 'Password Confirmation', 'trim|required|matches[password]');
		$this->form_validation->set_rules('fullname', 'Full Name', 'trim|required|min_length[3]|max_length[100]');
		$this->form_validation->set_rules('gender', 'Gender', 'required');
		$this->form_validation->set_rules('dob', 'Birthdate', 'trim|required|min_length[5]|max_length[50]');
		$this->form_validation->set_rules('phone', 'Phone Number', 'trim|required|min_length[5]|max_length[20]');
		$this->form_validation->set_rules('address', 'Address', 'trim|required|max_length[500]');

		if($this->form_validation->run() == FALSE) 
		{
			$this->session->set_flashdata('error', validation_errors());
			redirect('signup');
		}
		else
		{
			// create user if validation success
			$d = $this->input->post();
			$d['dob'] = date('Y-m-d', strtotime($d['dob']));
			unset($d['confirm-password']);

			if($this->users->create($d))
			{
				redirect('signup/success');
			}
			else 
			{
				$this->session->set_flashdata('msg', 'Oops! An error has occured, please contact our customer service :)');	
				redirect('signup');
			}
		}
	}

}
