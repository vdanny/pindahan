<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Backend extends CI_Controller 
{
	private $master = 'backend/master';

	function __construct()
	{
		parent::__construct();
		$this->load->model('Administrator');
	}

	public function index() 
	{
		$data['admins'] = $this->administrator->get();
		$data['content'] = 'backend/admin/index';
		$this->load->view($this->master, $data);
	}

}