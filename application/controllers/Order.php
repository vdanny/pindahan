<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('packages');
		$this->load->model('customprices');
		$this->load->model('appliances');
		$this->load->model('sizes');
		$this->load->model('orders');

		if ($this->session->has_userdata('user') == FALSE) {
			redirect('signin');
		}
	}

	public function index()
	{
		$data['packages'] = $this->packages->get_limit(4);
		$data['prices'] = $this->customprices->get();
		$data['appliances'] = $this->appliances->get();
		$data['distance_price'] = $this->orders->get_distance_price();
		$this->load->view('order', $data);
	}

	public function get_size($appliance_id)
	{
		$sizes = $this->sizes->get($appliance_id);
    	header('Content-Type: application/json');
    	echo json_encode($sizes);
	}

	public function check_moving_time()
	{
		$datetime = $this->input->post('datetime');
		$date = strtotime($datetime);

		$available = $this->orders->is_available_time($date);
    	header('Content-Type: application/json');
		echo json_encode(array('available' => $available));
	}

	public function get_order_by_date()
	{
		$date = $this->input->post('date');
		$d = strtotime($date);
		$orders = $this->orders->get_by_date($d);
		echo json_encode(array('orders' => $orders));
	}

	public function submit()
	{
		// setup $data
		$data = $this->input->post();

		$data['user_id'] = $this->session->userdata('user')->id;
		$moving_datetime = strtotime($data['moving_date'] . ' ' . $data['moving_time']);
		unset($data['moving_date']);
		unset($data['moving_time']);
		$data['moving_datetime'] = date('Y-m-d H:i:s', $moving_datetime);

		// separate custom prices into array
		$custom_id = $data['custom_id'];
		$custom = $data['custom']; // qty of custom service
		unset($data['custom_id']);
		unset($data['custom']);

		// separate appliances into array
		$appliances_id = isset($data['appliances_id'])? $data['appliances_id'] : array();
		$sizes_id = isset($data['sizes_id'])? $data['sizes_id'] : array();
		$appliances = isset($data['appliances'])? $data['appliances'] : array(); // qty of appliance
		unset($data['appliances_id']);
		unset($data['sizes_id']);
		unset($data['appliances']);

		$order = $this->orders->create($data);

		if($data['package_id'] == 0)
		{
			// if user choose custom, then insert custom price into order_details
			for ($i=0; $i < count($custom_id); $i++) 
			{ 
				if($custom[$i] != 0)
				{
					$this->orders->create_order_detail(
						array(
							'custom_id' => $custom_id[$i],
							'qty' => $custom[$i]
						)
					);
				}
			}
		}

		if(count($appliances) > 0)
		{
			// if user input appliances, then insert into appliance_details
			for ($i=0; $i < count($appliances_id); $i++) { 
				if($appliances[$i] != 0)
				{
					$this->orders->create_appliance_detail(
						array(
							'order_id' => $order->id, 
							'appliance_id' => $appliances_id[$i], 
							'size_id' => $sizes_id[$i],
							'qty' => $appliances[$i]
						)
					);
				}
			}
		}

		redirect('dashboard/index');
	}

}
