<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Banks extends CI_Model {

	private $table = 'banks';
	private $fields = 'id, bank_name, account_name, account_no, created_at, updated_at, modified_by';

	function __construct()
	{
		parent::__construct();
	}

	function get($id = 0) 
	{
		$this->db->select($this->fields);
		if($id != 0) 
		{
			$this->db->where('id', $id);
		}
		$query = $this->db->get($this->table);
		return $id == 0 ? $query->result() : $query->result()[0];
	}

	function create($data)
	{
		$data['created_at'] = date('Y-m-d H:i:s');
		return $this->db->insert($this->table, $data);
	}

	function update($data)
	{
		$this->db->set('bank_name', $data['bank_name']);
		$this->db->set('account_name', $data['account_name']);
		$this->db->set('account_no', $data['account_no']);
		$this->db->set('modified_by', $data['modified_by']);
		$this->db->where('id', $data['id']);
		return $this->db->update($this->table);
	}

	function delete($id)
	{
		$this->db->set('deleted_at', date('Y-m-d H:i:s'));
		$this->db->where('id', $id);
		return $this->db->update($this->table);
	}

}