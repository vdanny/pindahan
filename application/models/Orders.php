<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Orders extends CI_Model {

	private $table = 'orders';
	private $fields = 'id, user_id, fullname, phone, pickup_address, destination_address, moving_datetime, package_id, price, distance, payment_status, created_at, updated_at, modified_by';
	private $active = 'deleted_at IS NULL';

	private $detail_table = 'order_details';
	private $detail_fields = 'id, order_id, custom_id, qty, created_at, updated_at';

	private $appliance_table = 'appliance_details';
	private $appliance_fields = 'id, order_id, appliance_id, size_id, qty, created_at, updated_at';

	private $distance_table = 'distance_prices';
	private $distance_fields = 'id, price_per_km';

	function __construct()
	{
		parent::__construct();
		$this->load->model('packages');
		$this->load->model('customprices');
		$this->load->model('appliances');
		$this->load->model('sizes');
	}

	function get($id = 0) 
	{
		$this->db->select($this->fields);
		if($id != 0) 
		{
			$this->db->where('id', $id);
		}
		$this->db->where($this->active);
		$query = $this->db->get($this->table);
		$orders = $this->get_package($query->result());
		return $id == 0 ? $query->result() : $query->result()[0];
	}

	function get_orders_by_user($user_id)
	{
		$this->db->select($this->fields);
		$this->db->where('user_id', $user_id);
		$this->db->where($this->active);
		$this->db->order_by('created_at', 'DESC');
		$query = $this->db->get($this->table);
		$orders = $this->get_package($query->result());
		return $orders;
	}

	function get_last_order_by_user($user_id)
	{
		$this->db->select($this->fields);
		$this->db->where('user_id', $user_id);
		$this->db->where($this->active);
		$this->db->order_by('created_at', 'DESC');
		$row_num = $this->db->count_all_results($this->table);
		$query = $this->db->get($this->table);
		return $row_num > 0? $query->result()[0] : NULL;
	}

	function get_distance_price()
	{
		$this->db->select($this->distance_fields);
		$this->db->where('id', 1);
		$query = $this->db->get($this->distance_table);
		return $query->result()[0];
	}

	function get_by_date($date)
	{
		$d = date('Y-m-d H:i:s', $date);
		$this->db->select($this->fields);
		$this->db->where('moving_datetime BETWEEN \''.$d.'\' AND DATE_ADD(\''.$d.'\', INTERVAL 1 DAY)');
		$this->db->where($this->active);
		$query = $this->db->get($this->table);
		return $query->result();
	}

	function get_details($id)
	{
		$this->db->select($this->detail_fields);
		$this->db->where('order_id', $id);
		$this->db->where($this->active);
		$query = $this->db->get($this->detail_table);

		$details = $query->result();
		foreach ($details as $d) {
			$details->customprice = $this->customprices->get($d->custom_id);
		}
		return $details;
	}

	function get_appliance_details($order_id)
	{
		$this->db->select($this->appliance_fields);
		$this->db->where('order_id', $order_id);
		$this->db->where($this->active);
		$query = $this->db->get($this->appliance_table);

		$details = $query->result();
		$details = $this->get_appliance($details);
		$details = $this->get_size($details);
		
		return $details;
	}

	function is_available_time($start, $interval = 6)
	{
		$end = $start + ($interval * 60 * 60) - 60;

		if($start > strtotime(date('Y-m-d ', $start) . '20:00:00') || $start < strtotime(date('Y-m-d ', $start) . '07:00:00')) return false;

		$sql = 'SELECT * FROM '.$this->table.' WHERE \''.
			date('Y-m-d H:i:s', $start).'\' BETWEEN moving_datetime AND DATE_ADD(moving_datetime, INTERVAL '.$interval.' HOUR) OR \''.
			date('Y-m-d H:i:s', $end).'\' BETWEEN moving_datetime AND DATE_ADD(moving_datetime, INTERVAL '.$interval.' HOUR) AND '.$this->active;
		$query = $this->db->query($sql);
		return count($query->result()) == 0;
	}

	function create($data)
	{
		$data['created_at'] = date('Y-m-d H:i:s');
		$this->db->insert($this->table, $data);

		$this->db->select($this->fields);
		$this->db->where($this->active);
		$this->db->order_by('created_at', 'DESC');
		$this->db->limit(1);
		$query = $this->db->get($this->table);
		return $query->result()[0];
	}

	function create_order_detail($data)
	{
		$data['created_at'] = date('Y-m-d H:i:s');
		return $this->db->insert($this->detail_table, $data);
	}

	function create_appliance_detail($data)
	{
		$data['created_at'] = date('Y-m-d H:i:s');
		return $this->db->insert($this->appliance_table, $data);	
	}

	function update($data)
	{
		$this->db->set('fullname', $data['fullname']);
		$this->db->set('phone', $data['phone']);
		$this->db->set('pickup_address', $data['pickup_address']);
		$this->db->set('destination_address', $data['destination_address']);
		$this->db->set('package_id', $data['package_id']);
		$this->db->set('price', $data['price']);
		$this->db->set('distance', $data['distance']);
		$this->db->set('updated_at', date('Y-m-d H:i:s'));
		$this->db->where('id', $data['id']);
		return $this->db->update($this->table);
	}

	function update_status($id, $status)
	{
		$this->db->set('payment_status', $status);
		$this->db->where('id', $id);
		return $this->db->update($this->table);
	}

	function delete($id)
	{
		$this->db->set('deleted_at', date('Y-m-d H:i:s'));
		$this->db->where('id', $id);
		return $this->db->update($this->table);
	}

	function get_package($orders)
	{
		foreach ($orders as $o) {
			$o->package = $this->packages->get($o->package_id, TRUE);
		}
		return $orders;
	}

	function get_appliance($details)
	{
		foreach ($details as $d) {
			$d->appliance = $this->appliances->get($d->appliance_id);
		}
		return $details;
	}

	function get_size($details)
	{
		foreach ($details as $d) {
			$d->size = $this->sizes->get_by_id($d->size_id);
		}
		return $details;
	}

}