<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Administrators extends CI_Model {

	private $table = 'administrators';
	private $fields = 'id, username, password, is_super, created_at, updated_at';
	private $active = 'deleted_at IS NULL';

	function __construct()
	{
		parent::__construct();
	}

	function login($username, $password) 
	{
		$this->db->select($this->fields);
		$this->db->where('username', $username);
		$this->db->where('password', $password);
		$this->db->where($this->active);
		$query = $this->db->get($this->table);
		return $query->row();
	}

	function get($id = 0) 
	{
		$this->db->select($this->fields);
		if($id != 0) 
		{
			$this->db->where('id', $id);
		}
		$this->db->where($this->active);
		$query = $this->db->get($this->table);
		return $id == 0 ? $query->result() : $query->result()[0];
	}

	function create($data)
	{
		$data['created_at'] = date('Y-m-d H:i:s');
		return $this->db->insert($this->table, $data);
	}

	function update($data)
	{
		$this->db->set('username', $data['username']);
		if(isset($data['password'])) $this->db->set('password', $data['password']);
		$this->db->set('is_super', $data['is_super']);
		$this->db->where('id', $data['id']);
		return $this->db->update($this->table);
	}

	function delete($id)
	{
		$this->db->set('deleted_at', date('Y-m-d H:i:s'));
		$this->db->where('id', $id);
		return $this->db->update($this->table);
	}
}