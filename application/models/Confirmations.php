<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Confirmations extends CI_Model {

	private $table = 'confirmations';
	private $fields = 'id, order_id, bank_id, bank_name, account_name, amount, note, created_at, updated_at, modified_by';

	function __construct()
	{
		parent::__construct();
		$this->load->model('banks');
	}

	function get($id = 0) 
	{
		$this->db->select($this->fields);
		if($id != 0) 
		{
			$this->db->where('id', $id);
		}
		$query = $this->db->get($this->table);
		return $id == 0 ? $query->result() : $query->result()[0];
	}

	function get_by_order_id($order_id)
	{
		$this->db->select($this->fields);
		$this->db->where('order_id', $order_id);
		$query = $this->db->get($this->table);
		$confirm = $this->get_bank($query->result());
		return $confirm[0];
	}

	function create($data)
	{
		$payment_status = 1; // sudah dikonfirmasi
		$data['created_at'] = date('Y-m-d H:i:s');
		$this->orders->update_status($data['order_id'], $payment_status);
		return $this->db->insert($this->table, $data);
	}

	// function update($data)
	// {
	// 	$this->db->set('image_path', $data['image_path']);
	// 	$this->db->set('modified_by', $data['modified_by']);
	// 	$this->db->where('id', $data['id']);
	// 	return $this->db->update($this->table);
	// }

	function delete($id)
	{
		$this->db->where('id', $id);
		return $this->db->delete($this->table);
	}

	function get_bank($confirms)
	{
		foreach ($confirms as $c) {
			$c->bank = $this->banks->get($c->bank_id);
		}
		return $confirms;
	}

}