<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Packages extends CI_Model {

	private $table = 'packages';
	private $fields = 'id, name, transportation, distance, move_emp, pack_emp, unpack_emp, description, price, created_at, updated_at, modified_by';
	private $active = 'deleted_at IS NULL';

	function __construct()
	{
		parent::__construct();
	}

	function get($id = -1, $select_all = FALSE) 
	{
		$this->db->select($this->fields);
		if($id != -1) 
		{
			$this->db->where('id', $id);
		}
		if(!$select_all) $this->db->where($this->active);
		$query = $this->db->get($this->table);
		return $id == -1 ? $query->result() : $query->result()[0];
	}

	function get_limit($limit)
	{
		$this->db->select($this->fields);
		$this->db->where($this->active);
		$this->db->limit($limit);
		$query = $this->db->get($this->table);
		return $query->result();	
	}

	function create($data)
	{
		$data['created_at'] = date('Y-m-d H:i:s');
		return $this->db->insert($this->table, $data);
	}

	function update($data)
	{
		$this->db->set('name', $data['name']);
		$this->db->set('transportation', $data['transportation']);
		$this->db->set('distance', $data['distance']);
		$this->db->set('move_emp', $data['move_emp']);
		$this->db->set('pack_emp', $data['pack_emp']);
		$this->db->set('unpack_emp', $data['unpack_emp']);
		$this->db->set('description', $data['description']);
		$this->db->set('price', $data['price']);
		$this->db->set('updated_at', $data['updated_at']);
		$this->db->set('modified_by', $data['modified_by']);
		$this->db->where('id', $data['id']);
		return $this->db->update($this->table);
	}

	function delete($id)
	{
		$this->db->set('deleted_at', date('Y-m-d H:i:s'));
		$this->db->where('id', $id);
		return $this->db->update($this->table);
	}

}