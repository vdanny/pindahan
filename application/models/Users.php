<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Users extends CI_Model {

	private $table = 'users';
	private $fields = 'id, email, password, fullname, gender, dob, phone, address, is_merchant, company_name, created_at, updated_at';
	private $active = 'deleted_at IS NULL';

	function __construct()
	{
		parent::__construct();
	}

	function login($email, $password) 
	{
		$this->db->select($this->fields);
		$this->db->where('email', $email);
		$this->db->where('password', md5($password));
		$this->db->where($this->active);
		$query = $this->db->get($this->table);
		return $query->row();
	}

	function get($id = 0) 
	{
		$this->db->select($this->fields);
		if($id != 0) 
		{
			$this->db->where('id', $id);
		}
		$this->db->where($this->active);
		$query = $this->db->get($this->table);
		return $id == 0 ? $query->result() : $query->result()[0];
	}

	function create($data)
	{
		$data['password'] = md5($data['password']);
		$data['created_at'] = date('Y-m-d H:i:s');
		return $this->db->insert($this->table, $data);
	}

	function update($data)
	{
		$this->db->set('email', $data['email']); 
		if(isset($data['password']) && $data['password'] !== '') $this->db->set('password', md5($data['password'])); 
		$this->db->set('fullname', $data['fullname']);
		$this->db->set('dob', $data['dob']);
		$this->db->set('dob', $data['dob']);
		$this->db->set('phone', $data['phone']);
		$this->db->set('gender', $data['gender']);
		$this->db->set('address', $data['address']);
		$this->db->set('is_merchant', $data['is_merchant']);
		$this->db->set('company_name', $data['company_name']);
		$this->db->where('id', $data['id']);
		return $this->db->update($this->table);
	}

	function delete($id)
	{
		$this->db->set('deleted_at', date('Y-m-d H:i:s'));
		$this->db->where('id', $id);
		return $this->db->update($this->table);
	}
}