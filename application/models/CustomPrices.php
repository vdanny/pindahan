<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class CustomPrices extends CI_Model {

	private $table = 'custom_prices';
	private $fields = 'id, name, price, created_at, updated_at, modified_by';
	private $active = 'deleted_at IS NULL';

	function __construct()
	{
		parent::__construct();
	}

	function get($id = 0) 
	{
		$this->db->select($this->fields);
		if($id != 0) 
		{
			$this->db->where('id', $id);
		}
		$this->db->where($this->active);
		$query = $this->db->get($this->table);
		return $id == 0 ? $query->result() : $query->result()[0];
	}

	function create($data)
	{
		$data['created_at'] = date('Y-m-d H:i:s');
		return $this->db->insert($this->table, $data);
	}

	function update($data)
	{
		$this->db->set('name', $data['name']);
		$this->db->set('price', $data['price']);
		$this->db->set('updated_at', $data['updated_at']);
		$this->db->set('modified_by', $data['modified_by']);
		$this->db->where('id', $data['id']);
		return $this->db->update($this->table);
	}

	function delete($id)
	{
		$this->db->set('deleted_at', date('Y-m-d H:i:s'));
		$this->db->where('id', $id);
		return $this->db->update($this->table);
	}

}