// global variables
var price = 0;
var additionalPrice = 0;
var lastPackagePrice = 0;
var lastCustomPrice = 0;

// money formating
Number.prototype.formatMoney = function(c, d, t){
	var n = this, 
	    c = isNaN(c = Math.abs(c)) ? 2 : c, 
	    d = d == undefined ? "." : d, 
	    t = t == undefined ? "," : t, 
	    s = n < 0 ? "-" : "", 
	    i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", 
	    j = (j = i.length) > 3 ? j % 3 : 0;
   	return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
 };

// set grand total
 var setGrandTotal = function(amount) {
	$('#total-price').text('Rp ' + amount.formatMoney(2, ',', '.'));
 }

// refresh custom price when qty updated
 var evaluateTotalCustom = function() {
 	var customPrice = 0;
 	for (var i = 1; i <= $('#table-custom-price').children().length; i++) {
 		if(parseInt($('#subtotal-hidden-' + i).val()) > 0)
 			customPrice += parseInt($('#subtotal-hidden-' + i).val());
 	}
 	// count distancePrice
	var distance = parseInt($('#distance').val());
	var distancePrice = evaluateDistancePrice(distance, 0);
	setTransportationPrice(distancePrice);

 	lastCustomPrice = customPrice;
 	if(customPrice > 0){
		$('#price').val(customPrice + additionalPrice + distancePrice);
		$('#confirmation-price').text('Rp ' + customPrice.formatMoney(2, ',', '.'));
		setGrandTotal(customPrice + additionalPrice + distancePrice);
	}
	else {
		$('#price').val(0);
		$('#confirmation-price').text('-');
		setGrandTotal(additionalPrice + distancePrice);
	}
 }

var setTransportationPrice = function(price){
	if(price > 0)
		$('#transportation-price').text('Rp ' + price.formatMoney(2, ',', '.'));
	else
		$('#transportation-price').text('-');
}

// calculate distance price
var evaluateDistancePrice = function(distance, distanceFree) {
 	var distancePaid = distance > distanceFree? (distance-distanceFree) : 0;
 	if (distancePaid == 0) return 0;
 	else {
	 	// change distance unit from m to km
	 	var km = distancePaid / 1000;
	 	km = Math.ceil(km);
	 	var pricePerKM = parseInt($('#price_per_km').val());
	 	setTransportationPrice(pricePerKM * km);
	 	return pricePerKM * km;
	}
 }

$(document).ready(function() {

	// datepicker
	$('.datepicker').datepicker();

	/**
	 * REGION ORDER FORM
	 */
	// tabs on click event
	$('#package-price-tab').click(function() {
		$('#confirmation-price').text('Rp ' + lastPackagePrice.formatMoney(2, ',', '.'));
		var packageID = $('#package_id').val();
		if(packageID == "0" || packageID == undefined) {
			$('#package_id').val('');
			$('#price').val('0');	
		}
		setGrandTotal(lastPackagePrice + additionalPrice);
	});

	$('#custom-price-tab').click(function() {
		$('#confirmation-price').text('Rp ' + lastCustomPrice.formatMoney(2, ',', '.'));
		$('#package_id').val('0');
		$('#price').val('0');
		setGrandTotal(lastCustomPrice + additionalPrice);
	});

	// jquery validate
	$('#form-order').validate({
		ignore: [],
		messages: {
			price: {
				required: 'Harap pilih paket atau isi harga custom'
			},
			distance: {
				required: 'Harap isi prakiraan jarak terlebih dahulu'
			}
		}
	});

	// selecting shift
	// $('.btn-shift').click(function() {
	// 	var id = $(this).attr('data-id');
	// 	var value = $(this).text();
	// 	$('#moving-shift').val(id);
	// 	$('#confirmation-shift').text('Anda telah memilih shift ' + value);
	// });

	// selecting package
	$('.btn-package').click(function() {
		var id = $(this).attr('data-id');

		var freeDistance = parseInt($(this).attr('data-distance'));
		var distance = parseInt($('#distance').val());
		var distancePrice = evaluateDistancePrice(distance, freeDistance);
		setTransportationPrice(distancePrice);

		price = lastPackagePrice = parseInt($(this).attr('data-price'));
		$('#package_id').val(id);
		$('#price').val(price + additionalPrice + distancePrice);
		$('#confirmation-price').text('Rp ' + price.formatMoney(2, ',', '.'));
		setGrandTotal(price + additionalPrice + distancePrice);
	});

	// filling custom price
	$('.custom-qty').change(function() {
		var id = $(this).attr('data-id');
		var price = parseInt($(this).attr('data-price'));
		var qty = parseInt($(this).val());
		if((price*qty) > 0){
			$('#subtotal-' + id).text('Rp ' + (price*qty).formatMoney(2, ',', '.'));
			$('#subtotal-hidden-' + id).val(price*qty);
		}
		else{
			$('#subtotal-' + id).text('-');
			$('#subtotal-hidden-' + id).val(0);
		}
		evaluateTotalCustom();
	});

	function countApplianceRow() {
		return $('#appliance-row tr').length;
	}

	function checkDuplicateAppliance(appliance, size) {
		var applianceExists = false, sizeExists = false;
		$.each($('#appliance-row tr'), function(key, tr) {
			var $tr = $(tr);
			applianceExists = $tr.children()[0].innerHTML == appliance;
			sizeExists = $tr.children()[1].innerHTML == size;
			if(applianceExists && sizeExists) return false;
		});
		return !(applianceExists && sizeExists);
	}

	// add appliance row
	$('#btn-add-appliance').click(function() {
		checkDuplicateAppliance();
		if($('#appliances').val() != '' && $('#sizes').val() != '') {
			var appliance = $("#appliances option:selected").text();
			var size = $("#sizes option:selected").text();
			var appliance_id = $("#appliances").val();
			var size_id = $("#sizes").val();

			if(checkDuplicateAppliance(appliance, size)){
				$('#table-appliances').show();
				$('#no-appliances').hide();
				$('#appliance-row')
				.append('<tr><td>' + appliance + '</td><td>' + size + '</td><td>'+
					'<input type="hidden" name="appliances_id[]" value="' + appliance_id + '"/><input type="hidden" name="sizes_id[]" value="' + size_id + '"/>'+
					'<input type="number" name="appliances[]" value="0"></input></td>'+
					'<td><a class="btn btn-default btn-delete-row" title="Hapus"><i class="fa fa-trash"></i></button></td></tr>');

				// delete appliance row
				$('.btn-delete-row').click(function() {
					$(this).closest('tr').remove();
					if(countApplianceRow() == 0){
						$('#table-appliances').hide();
						$('#no-appliances').show();
					}
				});
			}
			else {
				alert('Pilihan perabotan dan ukuran tersebut sudah ada.');
			}
		}
		else {
			alert('Harap pilih perabotan dan ukuran terlebih dahulu.');
		}
	});

	/**
	 * END REGION ORDER FORM
	 */
	
	// jquery validate
	$('#form-signup').validate();

	// merchant name field
	$('#merchant').change(function() {
		if(this.checked) $('#company-name-div').show();
		else $('#company-name-div').hide();
	});
	
	// disable form submit on enter pressed
	$('#form-order').on('keyup keypress', function(e) {
		var keyCode = e.keyCode || e.which;
		if (keyCode === 13) { 
			e.preventDefault();
			return false;
		}
	});

});

/* Google Maps */

function initMap() {
    var origin_place_id = null;
    var destination_place_id = null;
    var origin_place_obj = null;
    var destination_place_obj = null;
    var travel_mode = google.maps.TravelMode.DRIVING;

    var map = new google.maps.Map(document.getElementById('map'), {
      mapTypeControl: false,
      scrollwheel: false,
      center: {lat: -33.8688, lng: 151.2195},
      zoom: 13
    });

    if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(function (position) {
			initialLocation = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
			map.setCenter(initialLocation);
		});
	}

    var distanceMatrixService = new google.maps.DistanceMatrixService();
    var directionsService = new google.maps.DirectionsService;
    var directionsDisplay = new google.maps.DirectionsRenderer;
    directionsDisplay.setMap(map);
    directionsDisplay.setOptions(
        {
            suppressPolylines: true,
            suppressMarkers: true
        }
    );

    var origin_input = document.getElementById('origin-input');
    var destination_input = document.getElementById('destination-input');
    var modes = document.getElementById('mode-selector');

    map.controls[google.maps.ControlPosition.TOP_LEFT].push(origin_input);
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(destination_input);
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(modes);

    var origin_autocomplete = new google.maps.places.Autocomplete(origin_input);
    origin_autocomplete.bindTo('bounds', map);

    var destination_autocomplete = new google.maps.places.Autocomplete(destination_input);
    destination_autocomplete.bindTo('bounds', map);

    function expandViewportToFitPlace(map, place) {
      if (place.geometry.viewport) {
        map.fitBounds(place.geometry.viewport);
      } else {
        map.setCenter(place.geometry.location);
        map.setZoom(17);
      }
    }

    var startMarkerUrl = 'http://www.google.com/mapfiles/dd-start.png';
    var endMarkerUrl = 'http://www.google.com/mapfiles/dd-end.png';
    var markers = [];
    function makeMarker( position, icon, title ) {
        var marker = new google.maps.Marker({
            position: position,
            map: map,
            icon: icon,
            title: title
        });
        markers.push(marker);
    }

    function deleteAllMarkers()
    {
    	while(markers.length){
            markers.pop().setMap(null);
        }
    }


    origin_autocomplete.addListener('place_changed', function() {
		var place = origin_place_obj = origin_autocomplete.getPlace();
		if (!place.geometry) {
			window.alert("Autocomplete's returned place contains no geometry");
			return;
		}
		expandViewportToFitPlace(map, place);

		// If the place has a geometry, store its place ID and route if we have
		// the other place ID
		origin_place_id = place.place_id;
		route(origin_place_id, destination_place_id, travel_mode, directionsService, directionsDisplay);
		getDistance(origin_place_obj, destination_place_obj);

		// change focus to destination-input
		$('#destination-input').focus();
    });

    destination_autocomplete.addListener('place_changed', function() {
		var place = destination_place_obj = destination_autocomplete.getPlace();
		if (!place.geometry) {
			window.alert("Autocomplete's returned place contains no geometry");
			return;
		}
		expandViewportToFitPlace(map, place);

		// If the place has a geometry, store its place ID and route if we have
		// the other place ID
		destination_place_id = place.place_id;
		route(origin_place_id, destination_place_id, travel_mode, directionsService, directionsDisplay);
		getDistance(origin_place_obj, destination_place_obj);
    });

    function route(origin_place_id, destination_place_id, travel_mode, directionsService, directionsDisplay) {
		if (!origin_place_id || !destination_place_id) {
			return;
		}
		deleteAllMarkers();
		directionsService.route(
			{
				origin: {'placeId': origin_place_id},
				destination: {'placeId': destination_place_id},
				travelMode: travel_mode
			}, 
			function(response, status) {
				if (status === google.maps.DirectionsStatus.OK) {
					directionsDisplay.setDirections(response);
					var leg = response.routes[ 0 ].legs[ 0 ];
					makeMarker( leg.start_location, startMarkerUrl, "Lokasi awal" );
					makeMarker( leg.end_location, endMarkerUrl, "Lokasi pindahan" );
				} else {
					window.alert('Directions request failed due to ' + status);
				}
			}
		);
    }

    function getDistance(origin_place, destination_place) {
        if(!origin_place_obj || !destination_place_obj) return;

        var origin = {lat: origin_place_obj.geometry.location.lat(), lng: origin_place_obj.geometry.location.lng()};
        var destination = {lat: destination_place_obj.geometry.location.lat(), lng: destination_place_obj.geometry.location.lng()};

        distanceMatrixService.getDistanceMatrix(
            {
                origins: [origin],
                destinations: [destination],
                travelMode: google.maps.TravelMode.DRIVING,
                avoidHighways: false,
                avoidTolls: true
            },
            getDistance_callback
        );
    }

    function getDistance_callback(response, status) {
        if(status == "OK"){
            console.log(response.rows[0].elements[0].distance);
            $('#distance-approx').html("Jarak yang akan ditempuh adalah " + response.rows[0].elements[0].distance.text);
            $('#distance').val(response.rows[0].elements[0].distance.value);
            // count distancePrice
			var distance = parseInt($('#distance').val());
			var distancePrice = evaluateDistancePrice(distance, 0);
			setTransportationPrice(distancePrice);
        }
        else {
            alert('status: ' + status);
        }
    }
}